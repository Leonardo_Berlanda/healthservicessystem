/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filters;

import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SSP;
import com.mycompany.persistence.entities.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author aless
 */
public class AuthenticationFilter implements Filter {

    private static int counter = 0;

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String contextPath = httpRequest.getContextPath();

        System.out.println("INSIDE DO FILTER " + counter++);
        if (session == null) {
            httpResponse.sendRedirect(contextPath + "/notAllowed.html");
        } else {
            String requestURI = httpRequest.getRequestURI();
            String userType = getUserType(session);
            System.out.println("USER TYPE: " + userType + " PAGE: " + requestURI.split("/")[requestURI.split("/").length - 1]);
            if (canUserAccess(userType, requestURI)) {
                System.out.println("USER GRANTED");
                httpResponse.setHeader("Cache-Control", "no-cache");
                httpResponse.setHeader("Pragma", "no-cache");
                httpResponse.setDateHeader("Expires", -1);
                filterChain.doFilter(httpRequest, httpResponse);
            } else {
                httpResponse.sendRedirect(contextPath + "/");
            }
        }
    }

    private boolean canUserAccess(String userType, String requestURI) {
        String pageRequested = requestURI.split("/")[requestURI.split("/").length - 1];

        String initialLetterOfPageName = pageRequested.substring(0, 1);
        return ("m".equals(initialLetterOfPageName.toLowerCase()) && "medic".equals(userType)
                || "p".equals(initialLetterOfPageName) && "patient".equals(userType)
                || "s".equals(initialLetterOfPageName) && "ssp".equals(userType))
                && userType != null;
    }

    private String getUserType(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user instanceof Medic) {
            return "medic";
        } else if (user instanceof Patient) {
            return "patient";
        } else if (user instanceof SSP) {
            return "ssp";
        }
        return null;
    }

    @Override
    public void destroy() {
    }

}
