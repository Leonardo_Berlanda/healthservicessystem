/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.MedicDAO;
import com.mycompany.persistence.entities.SSP;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mycompany.persistence.dao.SSPDAO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author aless
 */
@WebServlet(name = "SSPServlet", urlPatterns = {"/SSPServlet"})
public class SSPServlet extends HttpServlet {

    SSPDAO sspDao;

    @Override
    public void init() throws ServletException {
        super.init();

        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(SSPDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        SSP ssp = (SSP) session.getAttribute("user");

        String lastSubPath = getLastSubPath(request);
        System.out.println("LAST SUB PATH: " + lastSubPath);
        if (lastSubPath.equals("ssp")) {
            try {
                List<List<String>> allPatientsWithVisitToday = sspDao.getAllPatientsWithVisitsToday(ssp.getProvince());
                request.setAttribute("allPatientsWithVisitToday", allPatientsWithVisitToday);
                request.setAttribute("ssp", ssp);
                RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/SSP/MainPage.jsp"));
                dispatcher.forward(request, response);
            } catch (DAOException ex) {
                Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (lastSubPath.equals("getReport")) {
            //todo get the province

            String province = request.getParameter("province");
            System.out.println("PROVINCE: " + province);
            HSSFWorkbook wb = makeExcelReport(province);
            //todo send the file
            // this part is important to let the browser know what you're sending
            response.setContentType("application/vnd.ms-excel");
            // the next two lines make the report a downloadable file;
            // leave this out if you want IE to show the file in the browser window
            String fileName = "ResocontoFarmaci.xls";
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream out = response.getOutputStream();
            try {
                wb.write(out);
            } catch (IOException ioe) {
                System.out.println("ECCEZIONE: " + ioe.getMessage());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        SSP ssp = (SSP) session.getAttribute("user");

        String lastSubPath = getLastSubPath(request);

        if (lastSubPath.equals("doExam")) {
            BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));

            String data = br.readLine();
            System.out.println("DATA: " + data);
            int idAppointment = Integer.parseInt(data.split("=")[1]);
            try {
                sspDao.updateMadeInExamAppointment(idAppointment);
                response.flushBuffer();
            } catch (DAOException ex) {
                Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getLastSubPath(HttpServletRequest request) {
        String requestUri = request.getRequestURI();
        String[] requestPaths = requestUri.split("/");
        String lastSubPath = requestPaths[requestPaths.length - 1];
        lastSubPath = lastSubPath.split(";")[0];
        return lastSubPath;
    }

    private HSSFWorkbook makeExcelReport(String province) throws FileNotFoundException, IOException {
        try {
            String sheetName = "Report ricette erogate";//name of sheet

            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet(sheetName);

            List<String> params = new ArrayList<>();
            params.add("MEDICINA");
            params.add("QUANTITA'");
            params.add("NOME MEDICO");
            params.add("COGNOME MEDICO");
            params.add("NOME PAZIENTE");
            params.add("COGNOME PAZIENTE");
            HSSFRow rowParams = sheet.createRow(0);
            for (int i = 0; i < params.size(); i++) {
                HSSFCell cell = rowParams.createCell(i);
                cell.setCellValue(params.get(i));
            }

            List<List<String>> rows = sspDao.dataForReportMedicine(province);
            //iterating r number of rows
            for (int r = 0; r < rows.size(); r++) {
                HSSFRow row = sheet.createRow(r + 1);

                //iterating c number of columns
                for (int c = 0; c < 6; c++) {
                    HSSFCell cell = row.createCell(c);
                    cell.setCellValue(rows.get(r).get(c));
                }
            }
            return wb;
        } catch (DAOException ex) {
            Logger.getLogger(SSPServlet.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
