/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import javax.servlet.annotation.WebServlet;
import com.itextpdf.layout.Document;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.element.Paragraph;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Leona
 */
@WebServlet(name = "DownloadAnamnesisServlet", urlPatterns = {"/downloadanamnesisservlet"})
public class DownloadAnamnesisServlet extends HttpServlet {

    private PatientDAO patientDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDAO = daoFactory.getDAO(PatientDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = ((HttpServletRequest) request).getSession(false);

        String idSpecialisticVisit = request.getParameter("idSpecialisticVisit");
        String specialisticVisitName = request.getParameter("specialisticVisitName");
        String anamnesis = request.getParameter("anamnesis");
        String date = request.getParameter("date");
        SpecialisticVisitAppointment result = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(baos));
        Document doc = new Document(pdfDoc);
        doc.add(new Paragraph("TICKET VISITA SPECIALISTICA"));
        doc.add(new Paragraph("Data: " + date));
        doc.add(new Paragraph("Tipo visita: " + specialisticVisitName));
        doc.add(new Paragraph("Anamnesi: " + anamnesis));
        doc.add(new Paragraph("\n\n\n\n\n\n"));
        doc.add(new Paragraph("Costo prestazione: € 50,00" ));
        doc.close();

        // setting some response headers
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        // setting the content type
        response.setContentType("application/pdf");
        // the contentlength
        response.setContentLength(baos.size());
        // write ByteArrayOutputStream to the ServletOutputStream
        OutputStream os = response.getOutputStream();
        baos.writeTo(os);
        os.flush();
        os.close();

        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
        System.out.println(request.getContextPath() + "/patient.jsp");
        //dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
        System.out.println(request.getContextPath() + "/patient.jsp");
        dispatcher.forward(request, response);
    }
}
