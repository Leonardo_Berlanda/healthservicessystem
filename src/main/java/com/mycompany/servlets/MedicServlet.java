/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.google.gson.Gson;
import com.mycompany.persistence.dao.MedicDAO;
import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.ExamSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Medicine;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisit;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author aless
 */
public class MedicServlet extends HttpServlet {

    private MedicDAO medicDao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MedicServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MedicServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medicDao = daoFactory.getDAO(MedicDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            System.out.println("GET MEDIC SERVLET");
            HttpSession session = request.getSession(false);
            Medic medic = (Medic) session.getAttribute("user");
            String contextPath = request.getContextPath();
            String requestUri = request.getRequestURI();
            String[] requestPaths = requestUri.split("/");
            String lastSubPath = requestPaths[requestPaths.length - 1];
            String medicId = medic.getUserName();
            medic = medicDao.getMedicByMedicId(medic.getUserName());
            request.setAttribute("medic", medic);
            if (lastSubPath.equals("SchedaPaziente")) {
                String patientId = request.getParameter("patientId");
                if (patientId == null || !isPatientOfMedic(patientId, (List<Patient>) session.getAttribute("patients"))) {
                    response.sendRedirect(contextPath + "/notAllowed.html");
                } else {
                    try {
                        Patient patient = medicDao.getPatientByMedicAndPatientId(medicId, patientId);
                        request.setAttribute("patient", patient);
                        String visitsJson = medicDao.getVisitsByPatientId(patientId);
                        request.setAttribute("visitsJson", visitsJson);
                        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/Medic/PatientCard.jsp"));
                        dispatcher.forward(request, response);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("SchedaVisita")) {
                String patientId = request.getParameter("patientId");
                String visitId = request.getParameter("visitId");
                if (patientId == null || visitId == null || !isPatientOfMedic(patientId, (List<Patient>) session.getAttribute("patients"))) {
                    response.sendRedirect(contextPath + "/notAllowed.html");
                } else {
                    try {
                        Patient patient = medicDao.getPatientByMedicAndPatientId(medicId, patientId);
                        request.setAttribute("patient", patient);
                        String visitsJson = medicDao.getVisitsByPatientId(patientId);
                        String visit = medicDao.getVisitByPatientIdAndVisitId(patientId, Integer.parseInt(visitId));
                        request.setAttribute("visit", visit);
                        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/Medic/SchedaVisita.jsp"));
                        dispatcher.forward(request, response);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("CreaVisita")) {
                String patientId = request.getParameter("patientId");
                if (patientId == null || !isPatientOfMedic(patientId, (List<Patient>) session.getAttribute("patients"))) {
                    response.sendRedirect(contextPath + "/notAllowed.html");
                } else {
                    try {
                        request.setAttribute("contextPath", contextPath);
                        Patient patient = medicDao.getPatientByMedicAndPatientId(medicId, patientId);
                        request.setAttribute("patient", patient);
                        List<SpecialisticVisit> specialisticVisits = medicDao.getAllSpecialisticVisits();
                        String specialisticVisitsJson = new Gson().toJson(specialisticVisits);
                        request.setAttribute("specialisticVisits", specialisticVisitsJson);
                        List<Medicine> medicines = medicDao.getAllMedicines();
                        String medicinesJson = new Gson().toJson(medicines);
                        request.setAttribute("medicines", medicinesJson);
                        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/Medic/CreateVisit.jsp"));
                        dispatcher.forward(request, response);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("getSpecialisticVisitsBySubString")) {
                String subString = request.getParameter("subString");
                System.out.println("SUBSTRING: " + subString);
                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                if (subString == null) {
                    out.print("[]");
                    out.flush();
                } else {
                    try {
                        List<SpecialisticVisit> specialisticVisits = medicDao.getSpecialisticVisitsByStartSubString(subString);
                        String specialisticVisitsJson = new Gson().toJson(specialisticVisits);
                        out.print(specialisticVisitsJson);
                        out.flush();
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("getMedicinesByStartSubString")) {
                String subString = request.getParameter("subString");
                System.out.println("SUBSTRING: " + subString);
                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                if (subString == null) {
                    out.print("[]");
                    out.flush();
                } else {
                    try {
                        List<Medicine> medicines = medicDao.getMedicinesByStartSubString(subString);
                        String medicinesJson = new Gson().toJson(medicines);
                        out.print(medicinesJson);
                        out.flush();
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("getExamsSSPBySubString")) {
                String subString = request.getParameter("subString");
                System.out.println("SUBSTRING: " + subString);
                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                if (subString == null) {
                    out.print("[]");
                    out.flush();
                } else {
                    try {
                        List<ExamSSP> examsSSp = medicDao.getExamsSSPBySubString(subString);
                        String examsSSPJSON = new Gson().toJson(examsSSp);
                        out.print(examsSSPJSON);
                        out.flush();
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (lastSubPath.equals("SchedaVisitaSpecialistica")) {
                int specialisticVisitId = Integer.parseInt(request.getParameter("specialisticVisitId"));
                String patientId = request.getParameter("patientId");
                if (request.getParameter("specialisticVisitId") == null || patientId == null) {
                    response.sendRedirect(contextPath + "/notAllowed.html");
                } else {
                    request.setAttribute("contextPath", contextPath);

                    try {
                        Patient patient = medicDao.getPatientByPatientId(patientId);
                        request.setAttribute("patient", patient);
                    } catch (DAOException ex) {
                        Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/Medic/SpecialisticVisitCard.jsp"));
                    dispatcher.forward(request, response);

                }
            } else {
                try {
                    List<List<String>> patientsAndDates = medicDao.getPatientAndDatesByMedicId(medicId);
                    List<Patient> patients = medicDao.getPatientByMedicId(medicId);
                    session.setAttribute("patients", patients);
                    request.setAttribute("patientsAndDates", patientsAndDates);
                    RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/Medic/Medic.jsp"));
                    dispatcher.forward(request, response);
                } catch (DAOException ex) {
                    Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (DAOException ex) {
            Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isPatientOfMedic(String patientId, List<Patient> patientsOfMedic) {
        for (Patient patient : patientsOfMedic) {
            System.out.println("PATIENT ID: " + patient.getUserName() + " current patient: " + patientId);
            if (patient.getUserName().equals(patientId)) {
                return true;
            }
        }
        return false;
    }

    private String preparePatientsJsonArray(List<Patient> patients) {
        String json = "[";
        if (patients.size() > 0) {
            for (int i = 0; i < patients.size() - 1; i++) {
                json += patients.get(i).toString() + ", ";
            }
            int lastPatientIndex = patients.size() - 1;
            json += patients.get(lastPatientIndex).toString() + "]";
            return json;
        }
        return "[]";
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            System.out.println("REQUEST URI: " + request.getRequestURI());
            String requestUri = request.getRequestURI();
            String[] subPaths = requestUri.split("/");
            String lastSubPath = subPaths[subPaths.length - 1];
            System.out.println("LAST SUB PATH: " + lastSubPath);
            HttpSession session = request.getSession(false);
            Medic medic = (Medic) session.getAttribute("user");
            String province = medic.getProvince();
            String userName = medic.getUserName();
            String password = medic.getPassword();
            medic = medicDao.getMedicByMedicId(medic.getUserName());
            medic.setProvince(province);
            medic.setPassword(password);
            medic.setUserName(userName);
            request.setAttribute("medic", medic);
            if (lastSubPath.equals("MedicServlet")) {
                try {

                    Visit visit = new Visit();
                    visit.setDescription(request.getParameter("anamnesys"));
                    Date date = new Date(System.currentTimeMillis());
                    visit.setDateTime(date);
                    visit.setIdPatient(request.getParameter("idPatient"));
                    String idPatient = visit.getIdPatient();
                    System.out.println("ID PATIENT: " + visit.getIdPatient());
                    int visitId = medicDao.insertVisit(visit);

                    String textOfMail = "\nDESCRIZIONE VISITA\n" + visit.getDescription() + "\n\nVISITE\n";

                    String jsonString = request.getParameter("specialisticVisitAppointments");
                    System.out.println("JSON STRING: " + jsonString);
                    JSONArray json = new JSONArray(jsonString);
                    for (int i = 0; i < json.length(); i++) {
                        try {
                            JSONObject specialisticVisitObcject = json.getJSONObject(i);
                            SpecialisticVisitAppointment specialisticVisitAppointment = new SpecialisticVisitAppointment();

                            DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh : mm", Locale.ITALY);
                            date = df.parse(specialisticVisitObcject.getString("appointment"));
                            specialisticVisitAppointment.setDateTime(date);
                            specialisticVisitAppointment.setIdVisit(visitId);
                            String specialisticVisitName = specialisticVisitObcject.getString("specialisticVisitName");
                            medicDao.insertSpecialisticVisit(specialisticVisitAppointment, specialisticVisitName);
                            textOfMail += "Visita prescritta: " + specialisticVisitName + ", in data: " + specialisticVisitAppointment.getDateTime() + "\n";
                        } catch (ParseException e) {
                            System.out.println("INVALID DATE");
                        }
                    }

                    textOfMail += "\n\nFARMACI PRESCRITTI\n";

                    jsonString = request.getParameter("medicines");
                    System.out.println("JSON STRING: " + jsonString);
                    json = new JSONArray(jsonString);

                    for (int i = 0; i < json.length(); i++) {
                        JSONObject medicinePrescriptionJsonObcject = json.getJSONObject(i);
                        MedicinePrescription medicinePrescription = new MedicinePrescription();
                        medicinePrescription.setIdVisit(visitId);
                        medicinePrescription.setQuantity(medicinePrescriptionJsonObcject.getInt("quantity"));
                        String medicineName = medicinePrescriptionJsonObcject.getString("medicineName");
                        textOfMail += "Farmaco prescritto: " + medicineName + "\n";
                        medicDao.insertMedicinePrescription(medicinePrescription, medicineName);
                    }

                    textOfMail += "\n\nESAMI PRESCRITTI PRESSO SSP\n";

                    jsonString = request.getParameter("examSSPAppointments");
                    System.out.println("JSON STRING: " + jsonString);
                    json = new JSONArray(jsonString);
                    for (int i = 0; i < json.length(); i++) {
                        //TODO: inserire l'appuntamento per l'esame SSP
                        JSONObject examAppointmentJsonObject = json.getJSONObject(i);
                        ExamPrescribedAtSSP examPrescribedAtSSP = new ExamPrescribedAtSSP();
                        examPrescribedAtSSP.setVisitId(visitId);
                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh : mm", Locale.ITALY);
                        date = df.parse(examAppointmentJsonObject.getString("appointment"));
                        examPrescribedAtSSP.setExamDate(date);
                        String examName = examAppointmentJsonObject.getString("examName");
                        textOfMail += "Esame prescritto: " + examName + ", in data: " + examPrescribedAtSSP.getExamDate() + "\n";
                        medicDao.insertExamSSPAppointment(examPrescribedAtSSP, examName, province);
                    }

                    sendMailAfterVisit(idPatient, textOfMail);

                } catch (DAOException ex) {
                    Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JSONException ex) {
                    Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (lastSubPath.equals("SchedaVisitaSpecialistica")) {
                try {
                    String medicId = medic.getUserName();
                    String patientId = request.getParameter("patientId");
                    Patient patient = medicDao.getPatientByMedicAndPatientId(medicId, patientId);
                    request.setAttribute("patient", patient);
                    String visitsJson = medicDao.getVisitsByPatientId(patientId);
                    request.setAttribute("visitsJson", visitsJson);
                    int visitId = Integer.parseInt((String) session.getAttribute("visitId"));
                    request.setAttribute("visitId", visitId);

                    int specialisticVisitId = Integer.parseInt(request.getParameter("specialisticVisitId"));
                    String anamnesiVisita = request.getParameter("anamnesys");
                    String pagaDopo = request.getParameter("pagaDopo");
                    medicDao.updateSpecialisticVisit(specialisticVisitId, anamnesiVisita, pagaDopo);

                    response.sendRedirect("SchedaVisita?patientId=" + patientId + "&&visitId=" + visitId);
                } catch (DAOException ex) {
                    Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (lastSubPath.equals("payVisit")) {
                
                medicDao.updatePayed(Integer.parseInt(request.getParameter("idVisit")));
            }
        } catch (DAOException ex) {
            Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendMailAfterVisit(String idPatient, String textOfMail) throws DAOException {
        Patient patient = medicDao.getPatientByPatientId(idPatient);

        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "alessandro.grassi1998@gmail.com";
        final String password = "gjmzdbhfbumsqfgv";
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(patient.getEmail(), false));
            msg.setSubject("Risultato visita medica");
            msg.setText(textOfMail);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            //TODO: log the exception
            me.printStackTrace(System.err);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String visitId = request.getParameter("visitId");
        System.out.println("VISIT ID: " + visitId);
        session.setAttribute("visitId", visitId);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
