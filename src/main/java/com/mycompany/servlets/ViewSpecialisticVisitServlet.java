/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.google.gson.Gson;
import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Leona
 */
@WebServlet(name = "ViewSpecialisticVisitServlet", urlPatterns = {"/viewspecialisticvisitservlet"})
public class ViewSpecialisticVisitServlet extends HttpServlet {

    private PatientDAO patientDAO;
 

     @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDAO = daoFactory.getDAO(PatientDAO.class);

        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("OK");
        
        HttpSession session = request.getSession(false);
        String specialisticVisitId = request.getParameter("specialisticVisitId");
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        if (specialisticVisitId == null) {
            out.print("[]");
            out.flush();
        } else {
            try {
                System.out.println("HERE");
                SpecialisticVisitAppointment singleSpecialisticVisit;
                singleSpecialisticVisit = patientDAO.getSpecialisticSingleVisit(specialisticVisitId);
                System.out.println(singleSpecialisticVisit);
                System.out.println("Prima del Gson");
                String mySpecialisticVisitJson = new Gson().toJson(singleSpecialisticVisit);
                System.out.println("HEI"+mySpecialisticVisitJson);
                out.print(mySpecialisticVisitJson);
                System.out.println("CAO");
                out.flush();
            } catch (DAOException ex) {
                Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
