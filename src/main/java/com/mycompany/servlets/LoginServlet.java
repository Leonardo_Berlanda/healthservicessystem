/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.UserDAO;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.NoSuchProviderException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Leona
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/loginservlet"})
public class LoginServlet extends HttpServlet {

    private UserDAO userDao;
    private static HashMap<String, String> authenticatedUsers; // jsession + ID

    public LoginServlet() {
        super();
        authenticatedUsers = new HashMap<>();
    }

    public static String retrieveId(String jSessionId) {
        return authenticatedUsers.get(jSessionId);
    }

    @Override
    public void init() throws ServletException {

        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);

        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userType = request.getParameter("userType");
        String ricordami = request.getParameter("ricordami");

        String trueUserName = "";
        Cookie userTypeCookie = null;
        if (userType.compareTo("patient") == 0) {
            trueUserName = "P" + username;
            userTypeCookie = new Cookie("userType", "patient");
        } else if (userType.compareTo("medic") == 0) {
            trueUserName = "M" + username;
            userTypeCookie = new Cookie("userType", "medic");
        } else if (userType.compareTo("ssp") == 0) {
            trueUserName = "S" + username;
            userTypeCookie = new Cookie("userType", "ssp");
        }
        response.addCookie(userTypeCookie);
        System.out.println("Questo è l'username: " + trueUserName + ", password: " + password);

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        System.out.println("USERNAME: " + username);
        request.setAttribute("error", false);
        User user = searchForUser(trueUserName, password);
        if (user == null) {
            request.getSession().setAttribute("status", "not_found");
            HttpSession session = request.getSession(false);
            removeSession(session);
            request.setAttribute("error", true);
            request.getRequestDispatcher(response.encodeRedirectURL("/index.jsp")).forward(request, response);
            //response.sendRedirect(response.encodeRedirectURL(contextPath + "error.html"));
        } else {
            //addUserToCookie(user.getIdPaziente(), request, response);
            HttpSession session = request.getSession(false);
            removeSession(session);
            session = request.getSession();
            session.setAttribute("user", user);

            if (ricordami != null) {
                Cookie sessionCookie = new Cookie("JSESSIONID", session.getId());
                sessionCookie.setMaxAge(60*24*3600);
                response.addCookie(sessionCookie);
            }
            if (request.getSession(false) != null) {
                if (userType.compareTo("patient") == 0) {

                    response.sendRedirect(response.encodeRedirectURL(contextPath + "patient.jsp"));
                } else if (userType.compareTo("medic") == 0) {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicServlet"));
                } else if (userType.compareTo("ssp") == 0) {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp"));
                }
            } else {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            }

        }

    }

    private User searchForUser(String userName, String password) {
        try {
            return userDao.getByIdAndPassword(userName, password);
        } catch (DAOException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("OUTSIDE TRY CLOUSE IN SEARCH FOR USER()");
        return null;
    }

    private void removeSession(HttpSession session) {
        if (session != null) {
            User user = (User) session.getAttribute("user");
            removeSessionForParticularUser(session, user, "user");
        }

    }

    private void removeSessionForParticularUser(HttpSession session, Object user, String name) {
        if (user != null) {
            session.setAttribute(name, null);
            session.invalidate();
            user = null;
        }
    }

}
