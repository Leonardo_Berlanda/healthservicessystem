/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.MedicDAO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Medicine;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Leona
 */
@WebServlet(name = "PatientServlet", urlPatterns = {"/patientservlet"})
public class PatientServlet extends HttpServlet {

    private PatientDAO patientDAO;
    private MedicDAO medicDAO;
    private static HashMap<String, String> authenticatedUsers; // jsession + ID
    List<Medic> AllMedicAvailable = new ArrayList<>();
    List<Visit> AllVisit = new ArrayList<>();
    List<SpecialisticVisitAppointment> AllSpecialisticVisit = new ArrayList<>();
    List<ExamPrescribedAtSSP> AllExam = new ArrayList<>();
    List<MedicinePrescription> AllMedicine = new ArrayList<>();
   

    public PatientServlet() {
        super();
        authenticatedUsers = new HashMap<>();
    }

    public static String retrieveId(String jSessionId) {
        return authenticatedUsers.get(jSessionId);
    }

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDAO = daoFactory.getDAO(PatientDAO.class);

        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Patient patient = (Patient) session.getAttribute("user");
        if (patient.getUserName() == null) {
            response.sendRedirect("index.html");
            return;
        }

        try {
            String patientId = patient.getUserName();

            Patient dataPatients = patientDAO.getByPatient(patientId);

            String patientProvince = dataPatients.getProvince();

            Medic dataPatientsMedic = patientDAO.getMedicPatient(patientId);
            
            AllMedicAvailable = patientDAO.getAllMedicAvailable(patientProvince, dataPatientsMedic.getUserName());
            AllVisit = patientDAO.getAllVisit(patientId);
            AllSpecialisticVisit = patientDAO.getAllSpecialisticVisit(patientId);
            AllExam = patientDAO.getAllExam(patientId);
            AllMedicine = patientDAO.getAllMedicine(patientId);

            request.setAttribute("dataPatients", dataPatients);
            request.setAttribute("dataPatientsMedic", dataPatientsMedic);
            request.setAttribute("AllmedicAvailable", AllMedicAvailable);
            request.setAttribute("allVisit", AllVisit);
            request.setAttribute("allSpecialisticVisit", AllSpecialisticVisit);
            request.setAttribute("allExam", AllExam);

            request.setAttribute("allMedicine", AllMedicine);

            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
            System.out.println(request.getContextPath() + "/patient.jsp");
            dispatcher.forward(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(MedicServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Patient patient = (Patient) session.getAttribute("user");
        System.out.println(request.getParameter("bntU"));
        if (request.getParameter("btnM") != null) {
            String value = request.getParameter("btnM");
            int valueInt = Integer.parseInt(value);
            String medicId = AllMedicAvailable.get(valueInt - 1).getUserName();
            String patientId = patient.getUserName();
            try {
                patientDAO.changeMedic(medicId, patientId);
                response.sendRedirect(response.encodeRedirectURL("patient.jsp"));
            } catch (DAOException ex) {
                Logger.getLogger(PatientServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (request.getParameter("btnU") == null) {
            Part part = request.getPart("file");
            String fileName = "idutente";
            String savePath = "C:\\Users\\Leona\\Desktop\\Università\\Programmazione Web\\healthservicessystem\\var\\image" + File.separator + fileName;
            part.write(savePath + File.separator);
            try {
                response.sendRedirect(response.encodeRedirectURL("patient.jsp"));
            } catch (Exception ex) {
                Logger.getLogger(PatientServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /*private Patient searchForPatient(String userName) {
        try {
            return patientDAO.getByPatient(userName);
        } catch (DAOException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }*/
}
