/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Leona
 */
@WebServlet(name = "UpdatePhotoServlet", urlPatterns = {"/updatephotoservlet"})
public class UpdatePhotoServlet extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "C:\\Users\\aless\\Documents\\NetBeansProjects\\mavenproject2\\healthservicessystem\\src\\main\\webapp\\images\\profile";
    private PatientDAO patientDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDao = daoFactory.getDAO(PatientDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        HttpSession session = ((HttpServletRequest)request).getSession(false);
         Patient patient = (Patient) session.getAttribute("user");
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);

                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        String name = new File(item.getName()).getName();
                        if(name != null && name != ""){
                            name = "profile_picture_"+patient.getUserName()+".jpg";
                            item.write(new File(UPLOAD_DIRECTORY + File.separator + name));
                        }
                    }
                }                
            } catch (Exception ex) {
            }
        }
 
     
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(UpdatePhotoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect(response.encodeRedirectURL("patient.jsp"));
    }
}