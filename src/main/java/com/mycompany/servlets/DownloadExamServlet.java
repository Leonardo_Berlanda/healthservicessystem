/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Leona
 */
@WebServlet(name = "DownloadExamServlet", urlPatterns = {"/downloadexamservlet"})
public class DownloadExamServlet extends HttpServlet {
private PatientDAO patientDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDAO = daoFactory.getDAO(PatientDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = ((HttpServletRequest) request).getSession(false);


        String examName = request.getParameter("nameExam");
        String examDate = request.getParameter("dateExam");
        SpecialisticVisitAppointment result = null;
        /*try {
            result = risultatoDao.getRisultatoById(idRisultato);
        } catch (DAOException ex) {
            Logger.getLogger(DownloadRisultatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        } */
        String contextPath = getServletContext().getContextPath();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(baos));
        Document doc = new Document(pdfDoc);
         doc.add(new Paragraph("TICKET ESAME"));
        doc.add(new Paragraph("Data: " + examDate));
        doc.add(new Paragraph("Tipo esame: " + examName));
        doc.add(new Paragraph("\n\n\n\n\n\n"));
        doc.add(new Paragraph("Costo prestazione: € 11,00" ));
        doc.close();

        // setting some response headers
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        // setting the content type
        response.setContentType("application/pdf");
        // the contentlength
        response.setContentLength(baos.size());
        // write ByteArrayOutputStream to the ServletOutputStream
        OutputStream os = response.getOutputStream();
        baos.writeTo(os);
        os.flush();
        os.close();

        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
        System.out.println(request.getContextPath() + "/patient.jsp");
        //dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
        System.out.println(request.getContextPath() + "/patient.jsp");
        dispatcher.forward(request, response);
    }
}
