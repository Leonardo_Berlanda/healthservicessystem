/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
/**
 *
 * @author Leona
 */
@WebServlet(name = "DownloadRecipeServlet", urlPatterns = {"/downloadrecipeservlet"})
public class DownloadRecipeServlet extends HttpServlet {

    private PatientDAO patientDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            patientDAO = daoFactory.getDAO(PatientDAO.class);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        allOkay(request, response);
        HttpSession session = request.getSession(false);
        String prescriptionId = request.getParameter("prescriptionId");
        String medicineName = request.getParameter("medicineName");
        String medicineBrand = request.getParameter("medicineBrand");
        String medicineDate = request.getParameter("medicineDate");
        String medicineQuantity = request.getParameter("medicineQuantity");
        Patient patient = (Patient) session.getAttribute("user");
        if (patient.getUserName() == null) {
            response.sendRedirect("index.html");
            return;
        }

        try {
            String patientId = patient.getUserName();

            Patient dataPatients = patientDAO.getByPatient(patientId);
            Medic dataPatientsMedic = patientDAO.getMedicPatient(patientId);

            System.out.println(dataPatients);
            System.out.println(dataPatientsMedic);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(baos));
            Document doc = new Document(pdfDoc);
            
            
            String qrText = "ID MEDICO: " + dataPatientsMedic.getUserName()+ "\n"
                    + "ID UNIVOCO DELLA PRESCRIZIONE: " + prescriptionId + "\n"
                    + "CODICE FISCALE PAZIENTE: " + dataPatients.getFiscalCode()+ "\n"
                    + "NOME FARMACO: " + medicineName + "\n"
                    + "DATA PRESCRIZIONE: " + medicineDate;
            final byte[] qrCode = QRCode.from(qrText).to(ImageType.JPG).to(ImageType.PNG).withSize(500, 500).stream().toByteArray();;

            ImageData qrCodeData = ImageDataFactory.create(qrCode);
            Image qrCodeImage = new Image(qrCodeData);
            
            
            
            doc.add(new Paragraph("RICETTA PER FARMACO"));
            doc.add(new Paragraph("DETTAGLI PAZIENTE: \nNome: " + dataPatients.getName()
                    + "\nCognome: " + dataPatients.getSurname()
                    + "\nData di nascita: " + dataPatients.getBirthDate()
                    + "\nCodice Fiscale: " + dataPatients.getFiscalCode()));
            doc.add(new Paragraph("Il sottoscritto " + dataPatientsMedic.getName() + " " + dataPatientsMedic.getSurname() + " prescrive al paziente soprastante, la tale medicina: "
                    + medicineName + " in numero di " + medicineQuantity + " unità"));

            doc.add(qrCodeImage);
            // doc.add(new Paragraph("Anamnesi: " + anamnesis));
            doc.close();

            // setting some response headers
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the contentlength
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();

            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
            System.out.println(request.getContextPath() + "/patient.jsp");

        } catch (DAOException ex) {
            Logger.getLogger(DownloadAnamnesisServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        allOkay(request, response);
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(response.encodeRedirectURL("/patients/patientJSP.html"));
        System.out.println(request.getContextPath() + "/patient.jsp");
        dispatcher.forward(request, response);
    }

    private void allOkay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cp = getServletContext().getContextPath();
        if (!cp.endsWith("/")) {
            cp += "/";
        }

        HttpSession session = ((HttpServletRequest) request).getSession(false);
        User patient = (Patient) session.getAttribute("paziente");

    }
}
