/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.dao.UserDAO;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.NoSuchProviderException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author aless
 */
@WebServlet(name = "CambiaPasswordServlet", urlPatterns = {"/CambiaPasswordServlet"})
public class CambiaPasswordServlet extends HttpServlet {

    
    private UserDAO userDao;

    public CambiaPasswordServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
        
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            userDao = daoFactory.getDAO(UserDAO.class);

        } catch (DAOFactoryException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String username = request.getParameter("username");
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        System.out.println("Username: " + username + " oldPassword: "  + oldPassword + " newPassword: " + newPassword);
        try {
            User user = userDao.getByIdAndPassword(username, oldPassword);
            if(user == null){
                
                //TODO: send wrong old password exception
            } else {
                byte[] salt = EncryptionUtilities.getSalt();
                String ecryptedPassword = EncryptionUtilities.getSecurePassword(newPassword, salt.toString().getBytes());
                userDao.updatePasswordByUsername(username, ecryptedPassword, salt.toString());
                response.sendRedirect(response.encodeRedirectURL("patient.jsp"));
            }
        } catch (DAOException | NoSuchAlgorithmException | NoSuchProviderException | java.security.NoSuchProviderException ex) {
            Logger.getLogger(CambiaPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
