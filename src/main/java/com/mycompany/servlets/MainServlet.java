/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.servlets;

import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SSP;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author aless
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String contextPath = request.getContextPath();
        request.setAttribute("error", false);
        
        if (session == null) {
            request.getRequestDispatcher(response.encodeRedirectURL("/index.jsp")).forward(request, response);
        } else if (session.getAttribute("user") instanceof Medic) {
            response.sendRedirect(contextPath + "/MedicServlet");
        } else if (session.getAttribute("user") instanceof Patient) {
            response.sendRedirect(contextPath + "/patient.jsp");
        } else if (session.getAttribute("user") instanceof SSP) {
            response.sendRedirect(contextPath + "/ssp");
        } else {
            response.sendRedirect(contextPath + "/index.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
