/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author aless
 */
public class Patient extends User implements Serializable {
    private String name;
    private String surname;
    private String birthDate;
    private String birthLocation;
    private String fiscalCode;
    private String sex;
    private String email;
    private String patientPhotoPath;
    private String medicId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthLocation() {
        return birthLocation;
    }

    public void setBirthLocation(String birthLocation) {
        this.birthLocation = birthLocation;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPatientPhotoPath() {
        return patientPhotoPath;
    }

    public void setPatientPhotoPath(String patientPhotoPath) {
        this.patientPhotoPath = patientPhotoPath;
    }

    public String getMedicId() {
        return medicId;
    }

    public void setMedicId(String medicId) {
        this.medicId = medicId;
    }
    
    
    
    /*@Override
    public String toString() {
        String objectInJsonFormat = "{\"userName\": \"" + getUserName() + "\", \"name\": \"" + getName() 
                + "\", \"surname\": \"" + getSurname() + "\", \"birthDate\": \"" + getBirthDate() 
                +"\", \"birthLocation\": \"" + getBirthLocation() + "\", \"fiscalCode\": \"" + getFiscalCode() + "\", \"sex\": \"" + getSex() 
                + "\", \"email\": \"" + getEmail() + "\", \"patientPhotoPath\": \"" + getPatientPhotoPath()
                + "\", \"medicId\": \"" + getMedicId() + "\"}";
        return objectInJsonFormat;
    }*/

    @Override
    public String toString() {
        return "Patient{" + "name=" + name + ", surname=" + surname + ", birthDate=" + birthDate + ", birthLocation=" + birthLocation + ", fiscalCode=" + fiscalCode + ", sex=" + sex + ", email=" + email + ", patientPhotoPath=" + patientPhotoPath + ", medicId=" + medicId + '}';
    }
}
