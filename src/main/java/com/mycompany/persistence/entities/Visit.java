/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

import java.util.Date;

/**
 *
 * @author aless
 */
public class Visit {
    private int id;
    private Date dateTime;
    private String description;
    private String idPatient;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    @Override
    public String toString() {
        String objectInJsonFormat = "Visit{id: " + getId() + ", dateTime: " + getDateTime() + ", description: " + getDescription() 
                + ", idPatient: " + getIdPatient() + "}";
        return objectInJsonFormat;
    }
    
    
    
}
