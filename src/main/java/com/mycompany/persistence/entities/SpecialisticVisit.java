/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

/**
 *
 * @author aless
 */
public class SpecialisticVisit {
    private int pkidSpecialisticVisit;
    private String name;

    public int getId() {
        return pkidSpecialisticVisit;
    }

    public void setId(int pkidSpecialisticVisit) {
        this.pkidSpecialisticVisit = pkidSpecialisticVisit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "SpecialisticVisit{id: " + getId() + ", name: " + getName() + "}";
        return objectInJsonFormat;
    }
}
