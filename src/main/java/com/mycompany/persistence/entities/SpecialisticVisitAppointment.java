/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

import java.util.Date;

/**
 *
 * @author aless
 */
public class SpecialisticVisitAppointment extends SpecialisticVisit{
    private int id;
    private Date dateTime;
    private boolean payed;
    private String anamnesis;
    private int idVisit;
    private int idSpecialisticVisit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public int getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
    }

    public int getIdSpecialisticVisit() {
        return idSpecialisticVisit;
    }

    public void setIdSpecialisticVisit(int idSpecialisticVisit) {
        this.idSpecialisticVisit = idSpecialisticVisit;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "SpecialisticVisitAppointment{id: " + getId() + ", dateTime: " + getDateTime() + 
                ", payed: " + isPayed() 
                + ", anamnesis: " + getAnamnesis() + ", idVisit" + getIdVisit() + ", idSpecialisticVisit" + getIdSpecialisticVisit() + "}";
        return objectInJsonFormat;
    }
}
