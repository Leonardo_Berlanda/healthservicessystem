/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

import java.util.Date;

/**
 *
 * @author aless
 */
public class ExamPrescribedAtSSP extends ExamSSP{
    private int id;
    private Date examDate;
    private Boolean made;
    private int idExam;
    private int idSSP;
    private int visitId;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }
    
    public Boolean getMade() {
        return made;
    }

    public void setMade(Boolean made) {
        this.made = made;
    }

    public int getIdExam() {
        return idExam;
    }

    public void setIdExam(int idExam) {
        this.idExam = idExam;
    }

    public int getIdSSP() {
        return idSSP;
    }

    public void setIdSSP(int idSSP) {
        this.idSSP = idSSP;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "ExamPrescribedAtSSP{id: " + getId() + ", dateTime: " + getExamDate() + 
                ", idSSP: " + getIdSSP() 
                + ", visitId: " + getVisitId() + "}";
        return objectInJsonFormat;
    }
}
