/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

/**
 *
 * @author aless
 */
public class Medicine {
    private int pkMedicineId;
    private String name;
    private String description;
    private String brand;

    public int getId() {
        return pkMedicineId;
    }

    public void setId(int pkMedicineId) {
        this.pkMedicineId = pkMedicineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    
    
    
    @Override
    public String toString() {
        String objectInJsonFormat = "Medic{id: " + getId() + ", name: " + getName() 
                + ", description: " + getDescription() + ", brand: " + getBrand() +"}";
        return objectInJsonFormat;
    }
}
