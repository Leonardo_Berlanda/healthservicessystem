/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

/**
 *
 * @author aless
 */
public class Medic extends User {
    private String name;
    private String surname;
    private String city;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "Medic{userName: " + getUserName() + ", name: " + getName() 
                + ", surname: " + getSurname() + ", city: " + getCity() +", address: " + getAddress()+"}";
        return objectInJsonFormat;
    }
}
