/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

/**
 *
 * @author aless
 */
public class SSP extends User{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "SSP{userName: " + getUserName() + ", name: " + getName() +"}";
        return objectInJsonFormat;
    }
}