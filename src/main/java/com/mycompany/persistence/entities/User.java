/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

/**
 *
 * @author aless
 */
public class User {
    private String userName;
    private String password;
    private String province;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "User{userName: " + getUserName() + ", password: " + getPassword() + ", province: " + getProvince() + "}";
        return objectInJsonFormat;
    }
    
    
}
