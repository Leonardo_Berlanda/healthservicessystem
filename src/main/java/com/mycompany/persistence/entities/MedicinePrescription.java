/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities;

import java.util.Date;

/**
 *
 * @author aless
 */
public class MedicinePrescription extends Medicine {
    private int id;
    private int quantity;
    private Date DateTime;
    private int idVisit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDateTime() {
        return DateTime;
    }

    public void setDateTime(Date DateTime) {
        this.DateTime = DateTime;
    }

    public int getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
    }
    
    @Override
    public String toString() {
        String objectInJsonFormat = "MedicinePrescription{id: " + getId() + ", quantity: " + getQuantity() + ", dateTime: " + getDateTime() 
                + ", idVisit: " + getIdVisit() + "}";
        return objectInJsonFormat;
    }
}
