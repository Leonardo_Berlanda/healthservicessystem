    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.entities.dao.factories.jdbc;

import com.mycompany.persistence.entities.dao.factory.DAO;
import com.mycompany.persistence.entities.dao.factory.DAOFactory;
import com.mycompany.persistence.entities.dao.factory.DAOFactoryException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aless
 */
public class JDBCDAOFactory implements DAOFactory {

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    private transient Connection CON = null;
    private final transient HashMap<Class, DAO> DAO_CACHE;

    private static JDBCDAOFactory instance;

    public static DAOFactory getInstance() throws DAOFactoryException {
        System.out.println("getInstance()");
        if (instance == null) {
            throw new DAOFactoryException("DAOFactory not yet configured. Call DAOFactory.configure(String dbUrl) before use the class");
        }
        return instance;
    }

    private JDBCDAOFactory() throws DAOFactoryException {
        super();
        System.out.println("JDBCDAOFactory()");
        DAO_CACHE = new HashMap<>();
         try {
            Class.forName("com.mysql.jdbc.Driver"); 
            String jdbcURL = "jdbc:mysql://localhost:3306/health_services_system?user=root&password=cisco&serverTimezone=Europe/Rome" ;
           // String url = "jdbc:mysql://localhost:3306/health_services_system";
            //Properties props = new Properties();
            //props.setProperty("user", "root");
            //props.setProperty("password", "cisco");
        
           
            CON = DriverManager.getConnection(jdbcURL);
            System.out.println("DATABASE CONNECTED!");
        } catch (SQLException ex) {
            System.out.println("DATABASE NOT CONNECTED!");

            Logger.getLogger(JDBCDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JDBCDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*String DRIVER = "com.mysql.jdbc.Driver";
       
       // String DBURL = "jdbc:mysql://localhost:3306/health_services_system";
        String DBURL = "jdbc:mysql://localhost/helath_services_system?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        
        String USER = "root";
       
        String PASS = "cisco";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            CON = DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        */
    }

    public static void configure() throws DAOFactoryException {
        System.out.println("configure()");
        if (instance == null) {
            instance = new JDBCDAOFactory();
        } else {
            throw new DAOFactoryException("DAOFactory already configured. You can call configure only one time");
        }
    }

    @Override
    public void shutdown() {
        try {
            CON.close();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoInterface) throws DAOFactoryException {
        System.out.println("getDAO()");
        DAO dao = DAO_CACHE.get(daoInterface);
        if (dao != null) {
            return (DAO_CLASS) dao;
        }

        Package pkg = daoInterface.getPackage();
        String prefix = pkg.getName() + ".jdbc.JDBC";
        try {
            System.out.println("prefix: " + prefix + " simpeName of DAO interface: " + daoInterface.getSimpleName());
            Class daoClass = Class.forName(prefix + daoInterface.getSimpleName());

            Constructor<DAO_CLASS> constructor = daoClass.getConstructor(Connection.class);
            DAO_CLASS daoInstance = constructor.newInstance(CON);

            if (!(daoInstance instanceof JDBCDAO)) {
                throw new DAOFactoryException("The daoInterface passed as parameter doesn't extend JDBCDAO class");
            }

            DAO_CACHE.put(daoInterface, daoInstance);
            return daoInstance;
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | SecurityException ex) {
            throw new DAOFactoryException("Impossible to return the DAO", ex);
        }
    }
}
