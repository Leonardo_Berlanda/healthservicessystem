/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao;

import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factory.DAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.util.List;

/**
 *
 * @author Leona
 */
public interface PatientDAO extends DAO<Patient, String>{
    public Patient getByPatient(String userName) throws DAOException;
    public Medic getMedicPatient(String userName) throws DAOException;
    public List<Medic> getAllMedicAvailable(String province, String patientId) throws DAOException;
    public void changeMedic(String medicId, String patientId) throws DAOException;
    public List<Visit> getAllVisit(String userName) throws DAOException;
    public List<SpecialisticVisitAppointment> getAllSpecialisticVisit(String userName) throws DAOException;
    public List<ExamPrescribedAtSSP> getAllExam(String userName) throws DAOException;
    public List<MedicinePrescription> getAllMedicine(String userName) throws DAOException;
    public Visit getSingleVisit(String visitId) throws DAOException;
    public SpecialisticVisitAppointment getSpecialisticSingleVisit (String specialisticVisitId) throws DAOException;
}

