/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao;

import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.ExamSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Medicine;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisit;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factory.DAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.util.List;

/**
 *
 * @author aless
 */
public interface MedicDAO extends DAO<Medic, String> {
    public List<Patient> getPatientByMedicId(String medicId) throws DAOException;
    public List<List<String>> getPatientAndDatesByMedicId(String medicId) throws DAOException;
    public Patient getPatientByMedicAndPatientId(String medicId, String patientId) throws DAOException;
    public String getVisitsByPatientId(String patientId) throws DAOException;
    public List<Medicine> getAllMedicines() throws DAOException;
    public List<Medicine> getMedicinesByStartSubString(String startSubString) throws DAOException;
    public List<SpecialisticVisit> getAllSpecialisticVisits() throws DAOException;
    public List<SpecialisticVisit> getSpecialisticVisitsByStartSubString(String startSubString) throws DAOException;
    public List<ExamSSP> getExamsSSPBySubString(String sybString) throws DAOException;
    public int insertVisit(Visit visit) throws DAOException;
    public void insertSpecialisticVisit(SpecialisticVisitAppointment specialistiVisitAppointmen, String specialisticVisitName) throws DAOException;
    public void insertMedicinePrescription(MedicinePrescription medicinePrescription, String medicineName) throws DAOException;
    public void updateSpecialisticVisit(int specialisticVisitId, String anamnesys, String pagaDopo) throws DAOException;
    public Patient getPatientByPatientId(String patientId) throws DAOException;
    public String getVisitByPatientIdAndVisitId(String patientId, int visitId) throws DAOException;
    public Medic getMedicByMedicId(String medicId) throws DAOException;
    public void insertExamSSPAppointment(ExamPrescribedAtSSP examPrescribedAtSSP, String examName, String province) throws DAOException;
    public void updatePayed(int specialisticVisitAppointmentId) throws DAOException;
}
