/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao.jdbc;

import com.mycompany.persistence.dao.MedicDAO;
import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.ExamSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Medicine;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisit;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factories.jdbc.JDBCDAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aless
 */
public class JDBCMedicDAO extends JDBCDAO<Medic, String> implements MedicDAO {

    private String GET_MEDIC_BY_MEDIC_ID = "SELECT * FROM health_services_system.medic\n"
            + "WHERE user_idUser = ?;";

    private String getPatientsByMedicId = "SELECT user_idUser, name, surname, fiscal_code, email, medic_id, province FROM health_services_system.patient JOIN user ON user.username = patient.user_idUser WHERE medic_id = ?;";

    private String GET_LAST_VISIT_DATE = "SELECT MAX(v.date_time) as LastVisit "
            + "FROM patient p "
            + "LEFT JOIN visit v ON v.id_patient = p.user_idUser "
            + "WHERE p.user_idUser = ? "
            + "AND p.medic_id = ? "
            + "ORDER BY user_idUser;";
    private String GET_LAST_PRESCRIPTION_DATE = "SELECT MAX(v.date_time) as LastPrescription "
            + "FROM patient p "
            + "LEFT JOIN visit v ON v.id_patient = p.user_idUser "
            + "LEFT JOIN medicine_prescription m ON v.id = m.id_visit "
            + "WHERE p.user_idUser = ? "
            + "AND p.medic_id = ? "
            + "ORDER BY user_idUser;";

    private String GET_PATIENT_BY_MEDIC_AND_PATIENT_ID = "SELECT user_idUser, name, surname, birth_date, fiscal_code, sex, email, patient_photo_path \n"
            + "FROM health_services_system.patient\n"
            + "WHERE medic_id = ? AND user_idUser = ?";

    private String GET_VISITS_BY_PATIENT_ID = "SELECT JSON_OBJECT(\"visit_id\", v.id, \"date_time\", DATE_FORMAT(v.date_time, '%Y-%m-%d %T'),\"description\", v.description, \n" +
"            					\"medicines\", (select CAST(CONCAT('[',\n" +
"            											GROUP_CONCAT(\n" +
"            											JSON_OBJECT(\"medicineName\", m.name, \"id\", mp.id, \"quantity\", mp.quantity, \"date_time\", date_time)),\n" +
"                                                        ']') as JSON)\n" +
"            											FROM medicine_prescription mp\n" +
"                                                        JOIN medicine m ON mp.medicine_idmedicine = m.id\n" +
"                                                        WHERE v.id = mp.id_visit\n" +
"                                                        ),\n" +
"                                \"sepcialisticVisitsAppointments\", (select CAST(CONCAT('[',\n" +
"            														GROUP_CONCAT(\n" +
"            														JSON_OBJECT(\"specialisticVisitName\", sv.name, \"date\", DATE_FORMAT(sva.date_time_appointment, '%Y-%m-%d %T'), \"id\", sva.id, \"payed\", sva.payed, \"anamnesis\", sva.anamnesis)),\n" +
"                                                                    ']') as JSON)\n" +
"            														FROM specialistic_visit_appointment sva\n" +
"                                                                    JOIN specialistic_visit sv ON sva.id_specialistic_visit = sv.id\n" +
"                                                                    WHERE v.id = sva.id_visit),\n" +
"                                \"examsAppointments\", (select CAST(CONCAT('[',\n" +
"            														GROUP_CONCAT(\n" +
"            														JSON_OBJECT(\"examName\", es.name, \"date\", DATE_FORMAT(eps.exam_date, '%Y-%m-%d %T'), \"id\", eps.id, \"made\", eps.made)),\n" +
"                                                                    ']') as JSON)\n" +
"            														FROM exam_prescribed_at_issp eps\n" +
"                                                                    JOIN exam_ssp es ON eps.id_exam = es.id\n" +
"                                                                    WHERE v.id = eps.visit_id) ) as JSON\n" +
"            FROM visit v\n" +
"            WHERE id_patient = ?\n" +
"            ORDER BY UNIX_TIMESTAMP(v.date_time) desc;\n";

    private String GET_ALL_MEDICINES = "SELECT m.id, m.name FROM health_services_system.medicine m;";

    private String GET_ALL_MEDICINES_BY_SUB_STRING = "SELECT m.id, m.name FROM health_services_system.medicine m"
            + " WHERE m.name LIKE ? ESCAPE '!';";

    private String GET_EXAMSSSP_BY_SUB_STRING = "SELECT ssp.id, ssp.name FROM health_services_system.exam_ssp ssp"
            + " WHERE ssp.name LIKE ? ESCAPE '!';";

    private String GET_ALL_EXAMS = "SELECT sv.id, sv.name FROM health_services_system.specialistic_visit sv;";

    private String GET_ALL_EXAMS_BY_SUB_STRING = "SELECT sv.id, sv.name FROM health_services_system.specialistic_visit sv"
            + " WHERE sv.name LIKE ? ESCAPE '!';";

    private String INSERT_VISIT = "INSERT INTO `health_services_system`.`visit` (`date_time`, `description`, `id_patient`) VALUES (?, ?, ?);";

    private String GET_ID_SPECIALISTIC_VISIT_BY_NAME = "SELECT sv.id \n"
            + "FROM health_services_system.specialistic_visit sv \n"
            + "WHERE sv.name = ?;";

    private String INSERT_SPECIALISTIC_VISIT_APPOINTMENT = "INSERT INTO `health_services_system`.`specialistic_visit_appointment` (`date_time_appointment`, `payed`, `anamnesis`, `id_visit`, `id_specialistic_visit`) VALUES (?, '0', '', ?, ?);";

    private String INSERT_MEDICINE_PRESCRIPTION = "INSERT INTO `health_services_system`.`medicine_prescription` (`quantity` , `medicine_idmedicine`, `id_visit`) VALUES (?, ?, ?);";

    private String GET_MEDICINE_ID_BY_NAME = "SELECT m.id \n"
            + "FROM health_services_system.medicine m\n"
            + "WHERE m.name = ?;";

    private String GET_PATIENT_BY_PATIENT_ID = "SELECT * FROM health_services_system.patient p WHERE p.user_idUser = ?";

    private String UPDATE_SPECIALISTIC_VISIT = "UPDATE `health_services_system`.`specialistic_visit_appointment` SET `payed` = ?, `anamnesis` = ? WHERE (`id` = ?);";

    private String GET_VISIT_BY_PATIENT_ID_AND_VISIT_ID = "SELECT JSON_OBJECT(\"visit_id\", v.id, \"date_time\", DATE_FORMAT(v.date_time, '%Y-%m-%d %T'),\"description\", v.description, \n"
            + "            					\"medicines\", (select CAST(CONCAT('[',\n"
            + "            											GROUP_CONCAT(\n"
            + "            											JSON_OBJECT(\"medicineName\", m.name, \"id\", mp.id, \"quantity\", mp.quantity, \"date_time\", date_time)),\n"
            + "                                                        ']') as JSON)\n"
            + "            											FROM medicine_prescription mp\n"
            + "                                                        JOIN medicine m ON mp.medicine_idmedicine = m.id\n"
            + "                                                        WHERE v.id = mp.id_visit\n"
            + "                                                        ),\n"
            + "                                \"sepcialisticVisitsAppointments\", (select CAST(CONCAT('[',\n"
            + "            														GROUP_CONCAT(\n"
            + "            														JSON_OBJECT(\"specialisticVisitName\", sv.name, \"date\", DATE_FORMAT(sva.date_time_appointment, '%Y-%m-%d %T'), \"id\", sva.id, \"payed\", sva.payed, \"anamnesis\", sva.anamnesis)),\n"
            + "                                                                    ']') as JSON)\n"
            + "            														FROM specialistic_visit_appointment sva\n"
            + "                                                                    JOIN specialistic_visit sv ON sva.id_specialistic_visit = sv.id\n"
            + "                                                                    WHERE v.id = sva.id_visit),\n"
            + "                                \"examsAppointments\", (select CAST(CONCAT('[',\n"
            + "            														GROUP_CONCAT(\n"
            + "            														JSON_OBJECT(\"examName\", es.name, \"date\", DATE_FORMAT(eps.exam_date, '%Y-%m-%d %T'), \"id\", eps.id, \"made\", eps.made)),\n"
            + "                                                                    ']') as JSON)\n"
            + "            														FROM exam_prescribed_at_issp eps\n"
            + "                                                                    JOIN exam_ssp es ON eps.id_exam = es.id\n"
            + "                                                                    WHERE v.id = eps.visit_id) ) as JSON\n"
            + "            FROM visit v\n"
            + "            WHERE id_patient = ? and v.id = ?\n"
            + "            ORDER BY UNIX_TIMESTAMP(v.date_time) desc;";

    private String INSERT_EXAM_SSP_APPOINTMENT = "INSERT INTO `health_services_system`.`exam_prescribed_at_issp` (`exam_date`, `id_exam`, `id_ssp`, `visit_id`) VALUES (?, ?, ?, ?);";

    private String GET_ID_EXAM_BY_NAME = "SELECT id FROM exam_ssp WHERE name = ?;";

    private String GET_SSPID_BY_PROVINCE = "SELECT user_idUser as id FROM health_services_system.ssp WHERE name = ?;";
    
    private String UPDATE_VISIT_APPOINTMENT_PAYED_BY_ID = "UPDATE `health_services_system`.`specialistic_visit_appointment` SET `payed` = '1' WHERE (`id` = ?);";

    public JDBCMedicDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Medic getByPrimaryKey(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medic> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Patient> getPatientByMedicId(String medicId) throws DAOException {
        if (medicId == null) {
            throw new DAOException("medicId is a mandatory fields", new NullPointerException("medicId is null"));
        }

        try ( PreparedStatement stm = CON.prepareStatement(getPatientsByMedicId)) {
            stm.setString(1, medicId);
            try ( ResultSet rs = stm.executeQuery()) {
                List<Patient> patientsOfMedic = new ArrayList<>();
                while (rs.next()) {
                    Patient patient = new Patient();
                    patient.setUserName(rs.getString("user_idUser"));
                    patient.setName(rs.getString("name"));
                    patient.setSurname(rs.getString("surname"));
                    patient.setFiscalCode(rs.getString("fiscal_code"));
                    patient.setEmail(rs.getString("email"));
                    patient.setMedicId(rs.getString("medic_id"));
                    patientsOfMedic.add(patient);
                }
                if (patientsOfMedic.size() > 0) {
                    return patientsOfMedic;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<List<String>> getPatientAndDatesByMedicId(String medicId) throws DAOException {
        if (medicId == null) {
            throw new DAOException("medicId is a mandatory fields", new NullPointerException("medicId is null"));
        }

        List<List<String>> patientsOfMedic = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(getPatientsByMedicId)) {
            stm.setString(1, medicId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    List<String> patient = new ArrayList<String>();
                    patient.add(rs.getString("name"));
                    patient.add(rs.getString("surname"));
                    patient.add(rs.getString("fiscal_code"));
                    patient.add(rs.getString("email"));
                    patient.add(getLastVisitDate(medicId, rs.getString("user_idUser")));
                    patient.add(getLastPrescriptionDate(medicId, rs.getString("user_idUser")));
                    patient.add(rs.getString("user_idUser"));
                    patientsOfMedic.add(patient);
                }
                return patientsOfMedic;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    private String getLastVisitDate(String medicId, String idPatient) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(GET_LAST_VISIT_DATE)) {
            stm.setString(1, idPatient);
            stm.setString(2, medicId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    if (rs.getDate(1) == null) {
                        return "";
                    } else {
                        return rs.getDate(1).toString();
                    }
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
        return "";
    }

    private String getLastPrescriptionDate(String medicId, String idPatient) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(GET_LAST_PRESCRIPTION_DATE)) {
            stm.setString(1, idPatient);
            stm.setString(2, medicId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    if (rs.getDate(1) == null) {
                        return "";
                    } else {
                        return rs.getDate("LastPrescription").toString();
                    }
                }
            }
        } catch (SQLException ex) {
            try {
                throw new DAOException("Impossible to get the list of patients", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCMedicDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return "";
    }

    @Override
    public Patient getPatientByMedicAndPatientId(String medicId, String patientId) throws DAOException {
        Patient patient = new Patient();
        try ( PreparedStatement stm = CON.prepareStatement(GET_PATIENT_BY_MEDIC_AND_PATIENT_ID)) {
            stm.setString(1, medicId);
            stm.setString(2, patientId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    patient.setUserName(rs.getString("user_idUser"));
                    patient.setName(rs.getString("name"));
                    patient.setSurname(rs.getString("surname"));
                    patient.setFiscalCode(rs.getString("fiscal_code"));
                    patient.setEmail(rs.getString("email"));
                    patient.setBirthDate(rs.getString("birth_date"));
                    patient.setSex(rs.getString("sex"));
                    patient.setPatientPhotoPath(rs.getString("patient_photo_path"));

                    return patient;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
        return null;
    }

    @Override
    public String getVisitsByPatientId(String patientId) throws DAOException {
        String jsonToSend = "[";
        try ( PreparedStatement stm = CON.prepareStatement(GET_VISITS_BY_PATIENT_ID)) {
            stm.setString(1, patientId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    String visitJson = rs.getString("JSON");
                    jsonToSend += visitJson + ",";
                }
                jsonToSend = jsonToSend.substring(0, jsonToSend.length() - 1);//elimina la virgola alla fine
                return jsonToSend + "]";
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<Medicine> getAllMedicines() throws DAOException {
        List<Medicine> medicines = new ArrayList<>();
        try ( PreparedStatement stm = CON.prepareStatement(GET_ALL_MEDICINES)) {
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Medicine medicine = new Medicine();
                    medicine.setId(rs.getInt("id"));
                    medicine.setName(rs.getString("name"));
                    medicines.add(medicine);
                }
                return medicines;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<SpecialisticVisit> getAllSpecialisticVisits() throws DAOException {
        List<SpecialisticVisit> specialisticVisits = new ArrayList<>();
        try ( PreparedStatement stm = CON.prepareStatement(GET_ALL_MEDICINES)) {
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    SpecialisticVisit specialisticVisit = new SpecialisticVisit();
                    specialisticVisit.setId(rs.getInt("id"));
                    specialisticVisit.setName(rs.getString("name"));
                    specialisticVisits.add(specialisticVisit);
                }
                return specialisticVisits;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<Medicine> getMedicinesByStartSubString(String subString) throws DAOException {
        List<Medicine> medicines = new ArrayList<>();
        subString = subString
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");
        try ( PreparedStatement stm = CON.prepareStatement(GET_ALL_MEDICINES_BY_SUB_STRING)) {
            stm.setString(1, subString + "%");
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Medicine medicine = new Medicine();
                    medicine.setId(rs.getInt("id"));
                    medicine.setName(rs.getString("name"));
                    medicines.add(medicine);
                }
                return medicines;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<SpecialisticVisit> getSpecialisticVisitsByStartSubString(String subString) throws DAOException {
        List<SpecialisticVisit> specialisticVisits = new ArrayList<>();
        subString = subString
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");
        try ( PreparedStatement stm = CON.prepareStatement(GET_ALL_EXAMS_BY_SUB_STRING)) {
            stm.setString(1, subString + "%");
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    SpecialisticVisit specialisticVisit = new SpecialisticVisit();
                    specialisticVisit.setId(rs.getInt("id"));
                    specialisticVisit.setName(rs.getString("name"));
                    specialisticVisits.add(specialisticVisit);
                }
                return specialisticVisits;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public int insertVisit(Visit visit) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(INSERT_VISIT, Statement.RETURN_GENERATED_KEYS)) {
            //stm.setDate(1, new java.sql.Date(visit.getDateTime().getTime()));
            //java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
            java.sql.Timestamp timeStamp = new java.sql.Timestamp(System.currentTimeMillis());
            stm.setTimestamp(1, timeStamp);
            stm.setString(2, visit.getDescription());
            stm.setString(3, visit.getIdPatient());
            stm.executeUpdate();
            ResultSet rs = stm.getGeneratedKeys();
            rs.next();
            int auto_id = rs.getInt(1);
            return auto_id;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public void insertMedicinePrescription(MedicinePrescription medicinePrescription, String medicineName) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(INSERT_MEDICINE_PRESCRIPTION)) {
            stm.setInt(1, medicinePrescription.getQuantity());
            stm.setInt(3, medicinePrescription.getIdVisit());

            try ( PreparedStatement stm2 = CON.prepareStatement(GET_MEDICINE_ID_BY_NAME)) {
                stm2.setString(1, medicineName);
                ResultSet rs = stm2.executeQuery();
                boolean exists = false;
                while (rs.next()) {
                    exists = true;
                    stm.setInt(2, rs.getInt("id"));
                }

                if (exists) {
                    stm.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public void insertSpecialisticVisit(SpecialisticVisitAppointment specialisticVisitAppointment, String specialisticVisitName) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(INSERT_SPECIALISTIC_VISIT_APPOINTMENT)) {
            stm.setTimestamp(1, new java.sql.Timestamp(specialisticVisitAppointment.getDateTime().getTime()));
            stm.setInt(2, specialisticVisitAppointment.getIdVisit());

            try ( PreparedStatement stm2 = CON.prepareStatement(GET_ID_SPECIALISTIC_VISIT_BY_NAME)) {
                stm2.setString(1, specialisticVisitName);
                ResultSet rs = stm2.executeQuery();
                boolean exists = false;
                while (rs.next()) {
                    exists = true;
                    stm.setInt(3, rs.getInt("id"));
                }

                if (exists) {
                    stm.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public void updateSpecialisticVisit(int specialisticVisitId, String anamnesys, String pagaDopo) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(UPDATE_SPECIALISTIC_VISIT)) {
            boolean idPagaDopo = false;
            if(pagaDopo == null){
                idPagaDopo = true;
            }
            stm.setBoolean(1, idPagaDopo);
            stm.setString(2, anamnesys);
            stm.setInt(3, specialisticVisitId);

            stm.executeUpdate();
            System.out.println("update specialistic visit, ID:" + specialisticVisitId + " anamnesys: " + anamnesys);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public Patient getPatientByPatientId(String patientId) throws DAOException {
        Patient patient = new Patient();
        try ( PreparedStatement stm = CON.prepareStatement(GET_PATIENT_BY_PATIENT_ID)) {
            stm.setString(1, patientId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    patient.setUserName(rs.getString("user_idUser"));
                    patient.setName(rs.getString("name"));
                    patient.setSurname(rs.getString("surname"));
                    patient.setFiscalCode(rs.getString("fiscal_code"));
                    patient.setSex(rs.getString("sex"));
                    patient.setEmail(rs.getString("email"));
                    patient.setBirthDate(rs.getString("birth_date"));
                    return patient;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public String getVisitByPatientIdAndVisitId(String patientId, int visitId) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(GET_VISIT_BY_PATIENT_ID_AND_VISIT_ID)) {
            stm.setString(1, patientId);
            stm.setInt(2, visitId);
            try ( ResultSet rs = stm.executeQuery()) {
                String visitJson = "";
                while (rs.next()) {
                    visitJson = rs.getString("JSON");
                }
                return visitJson;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public Medic getMedicByMedicId(String medicId) throws DAOException {
        Medic medic = new Medic();
        try ( PreparedStatement stm = CON.prepareStatement(GET_MEDIC_BY_MEDIC_ID)) {
            stm.setString(1, medicId);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    medic.setUserName(rs.getString("user_idUser"));
                    medic.setName(rs.getString("name"));
                    medic.setSurname(rs.getString("surname"));
                    medic.setAddress(rs.getString("address"));
                    medic.setCity(rs.getString("city"));
                }
                return medic;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

    }

    @Override
    public List<ExamSSP> getExamsSSPBySubString(String subString) throws DAOException {
        List<ExamSSP> examsSSP = new ArrayList<>();
        subString = subString
                .replace("!", "!!")
                .replace("%", "!%")
                .replace("_", "!_")
                .replace("[", "![");
        try ( PreparedStatement stm = CON.prepareStatement(GET_EXAMSSSP_BY_SUB_STRING)) {
            stm.setString(1, subString + "%");
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    ExamSSP examSSP = new ExamSSP();
                    examSSP.setId(rs.getInt("id"));
                    examSSP.setName(rs.getString("name"));
                    examsSSP.add(examSSP);
                }
                return examsSSP;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public void insertExamSSPAppointment(ExamPrescribedAtSSP examPrescribedAtSSP, String examName, String province) throws DAOException {
        System.out.println("\n examName:" + examName + " examPrescribedAtSSp" + examPrescribedAtSSP.getExamDate() + " province: " + province);
        try ( PreparedStatement stm = CON.prepareStatement(INSERT_EXAM_SSP_APPOINTMENT)) {
            stm.setTimestamp(1, new java.sql.Timestamp(examPrescribedAtSSP.getExamDate().getTime()));
            stm.setInt(4, examPrescribedAtSSP.getVisitId());
            try ( PreparedStatement stm2 = CON.prepareStatement(GET_ID_EXAM_BY_NAME)) {
                stm2.setString(1, examName);
                ResultSet rs = stm2.executeQuery();
                boolean exists = false;
                while (rs.next()) {
                    exists = true;
                    stm.setInt(2, rs.getInt("id"));
                    System.out.println("stm.setInt(2, " + rs.getInt("id") + ")");
                }
                PreparedStatement stm3 = CON.prepareStatement(GET_SSPID_BY_PROVINCE);
                stm3.setString(1, province);
                rs = stm3.executeQuery();
                if (exists) {
                    System.out.println("If exists ");
                    exists = false;
                    while (rs.next()) {
                        exists = true;
                        stm.setString(3, rs.getString("id"));
                        System.out.println("FINE SECONDO WHILE");
                    }
                }
                if (exists) {
                    System.out.println("INSERITO");
                    stm.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
        System.out.println("\n");
    }

    @Override
    public void updatePayed(int specialistiVisitAppointmentId) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(UPDATE_VISIT_APPOINTMENT_PAYED_BY_ID)) {
            stm.setInt(1, specialistiVisitAppointmentId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }
}
