/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao.jdbc;

import com.mycompany.persistence.dao.UserDAO;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SSP;
import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factories.jdbc.JDBCDAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import com.mycompany.servlets.EncryptionUtilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aless
 */
public class JDBCUserDAO extends JDBCDAO<User, String> implements UserDAO {

    private String GET_BY_USERNAME_PASSWORD = "SELECT * FROM user WHERE username = ? AND password = ?;";

    private String GET_SALT_BY_USERNAME = "SELECT salt FROM health_services_system.user WHERE username = ?;";
    
    private String CHANGE_PASSWORD_BY_USERNAME = "UPDATE `health_services_system`.`user` SET `password` = ?, `salt` = ? WHERE (`username` = ?);";

    public JDBCUserDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User getByPrimaryKey(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User getByIdAndPassword(String userName, String password) throws DAOException {
        if ((userName == null) || (password == null)) {
            throw new DAOException("id and password are mandatory fields", new NullPointerException("email or password are null"));
        }

        try ( PreparedStatement stm1 = CON.prepareStatement(GET_SALT_BY_USERNAME)) {
            stm1.setString(1, userName);
            String salt = "";
            try ( ResultSet rs1 = stm1.executeQuery()) {
                while (rs1.next()){
                    salt = rs1.getString("salt");
                }
                if(salt.equals("")){
                    return null;
                }
                password = EncryptionUtilities.getSecurePassword(password, salt.getBytes());
                System.out.println("USERNAME: " + userName +  " PASSWORD ENCRYPTED: " + password + " SALT: " + salt);
                try ( PreparedStatement stm = CON.prepareStatement(GET_BY_USERNAME_PASSWORD)) {
                    stm.setString(1, userName);
                    stm.setString(2, password);
                    try ( ResultSet rs = stm.executeQuery()) {
                        int count = 0;
                        while (rs.next()) {
                            count++;
                            if (count > 1) {
                                throw new DAOException("Unique constraint violated! There are more than one user with the same username! WHY???");
                            }
                            User user;
                            switch (userName.charAt(0)) {
                                case 'M':
                                    System.out.println("CASE: M");
                                    user = new Medic();
                                    break;
                                case 'P':
                                    System.out.println("CASE: P");
                                    user = new Patient();
                                    break;
                                case 'S':
                                    System.out.println("CASE: S");
                                    user = new SSP();
                                    break;
                                default:
                                    return null;
                            }
                            System.out.println("DOPO SWITCH CASE");
                            user.setUserName(rs.getString("username"));
                            user.setPassword(rs.getString("password"));
                            user.setProvince(rs.getString("province"));
                            return user;
                        }
                        System.out.println("FUORI DAL WHILE");
                        return null;
                    }
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public void updatePasswordByUsername(String username, String newPassword, String salt) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(CHANGE_PASSWORD_BY_USERNAME)) {
            stm.setString(1, newPassword);
            stm.setString(2, salt);
            stm.setString(3, username);
            stm.executeUpdate();
            System.out.println("DOPO EXECUTE UPDATE, NEW PASSWORD: " + newPassword + " SALT: " + salt + " ussername: " + username);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }
}
