/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao.jdbc;

import com.mycompany.persistence.dao.PatientDAO;
import com.mycompany.persistence.entities.ExamPrescribedAtSSP;
import com.mycompany.persistence.entities.Medic;
import com.mycompany.persistence.entities.Medicine;
import com.mycompany.persistence.entities.MedicinePrescription;
import com.mycompany.persistence.entities.Patient;
import com.mycompany.persistence.entities.SpecialisticVisitAppointment;
import com.mycompany.persistence.entities.Visit;
import com.mycompany.persistence.entities.dao.factories.jdbc.JDBCDAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leona
 */
public class JDBCPatientDAO extends JDBCDAO<Patient, String> implements PatientDAO {

    private String GET_BY_PATIENT = "SELECT * FROM patient JOIN user U1 ON patient.user_idUser = U1.username WHERE patient.user_idUser = ? ;";
    private String GET_MEDIC_PATIENT = "SELECT user.username, medic.name, medic.surname, medic.address, medic.city from medic JOIN user ON medic.user_idUser = user.username JOIN patient ON patient.medic_id = medic.user_idUser WHERE patient.user_idUser = ?;";
    private String GET_ALL_MEDIC_AVAILABLE = "SELECT * FROM medic JOIN user U1 ON medic.user_idUser = U1.username where U1.province = ? and medic.user_idUser != ?;";
    private String CHANGE_MEDIC = "UPDATE patient SET medic_id = ? WHERE user_idUser = ?;";
    private String GET_ALL_VISIT = "SELECT * FROM visit WHERE id_patient = ?;";
    private String GET_ALL_SPECIALISTIC_VISIT = "SELECT * FROM specialistic_visit_appointment JOIN visit ON visit.id = specialistic_visit_appointment.id_visit JOIN specialistic_visit ON specialistic_visit.id = specialistic_visit_appointment.id_specialistic_visit AND visit.id_patient = ?";
    private String GET_ALL_EXAM = "SELECT * FROM exam_prescribed_at_issp JOIN visit ON  visit.id = exam_prescribed_at_issp.visit_id JOIN exam_ssp ON exam_ssp.id = exam_prescribed_at_issp.id_exam AND visit.id_patient = ?;";
    private String GET_ALL_MEDICINE = "SELECT medicine_prescription.id, medicine.name, medicine.brand, medicine_prescription.quantity, visit.date_time as date_time FROM medicine_prescription JOIN medicine on medicine.id = medicine_prescription.medicine_idmedicine JOIN visit ON medicine_prescription.id_visit = visit.id JOIN patient ON visit.id_patient = patient.user_idUser WHERE visit.id_patient = ?;";
    private String GET_SINGLE_VISIT = "SELECT * FROM visit WHERE id = ?;";
    private String GET_SINGLE_SPECIALISTIC_VISIT = "select specialistic_visit_appointment.date_time_appointment, specialistic_visit_appointment.payed, specialistic_visit_appointment.anamnesis, specialistic_visit.name FROM specialistic_visit_appointment JOIN specialistic_visit ON specialistic_visit.id = specialistic_visit_appointment.id_specialistic_visit AND  specialistic_visit_appointment.id = ?";
    public JDBCPatientDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Patient getByPrimaryKey(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Patient> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Patient getByPatient(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_BY_PATIENT)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Patient patient = new Patient();
                    patient.setUserName(rs.getString("username"));
                    patient.setName(rs.getString("name"));
                    patient.setSurname(rs.getString("surname"));
                    patient.setBirthDate(rs.getString("birth_Date"));
                    patient.setBirthLocation(rs.getString("birth_Location"));
                    patient.setFiscalCode(rs.getString("fiscal_Code"));
                    patient.setSex(rs.getString("sex"));
                    patient.setEmail(rs.getString("email"));
                    patient.setPatientPhotoPath(rs.getString("patient_photo_path"));
                    patient.setMedicId(rs.getString("medic_Id"));
                    patient.setProvince(rs.getString("province"));

                    return patient;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public Medic getMedicPatient(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_MEDIC_PATIENT)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Medic medic = new Medic();
                    medic.setUserName(rs.getString("username"));
                    medic.setName(rs.getString("name"));
                    medic.setSurname(rs.getString("surname"));
                    medic.setAddress(rs.getString("address"));
                    medic.setCity(rs.getString("city"));

                    return medic;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public List<Medic> getAllMedicAvailable(String province, String patientId) throws DAOException {
        if (province == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_ALL_MEDIC_AVAILABLE)) {
            stm.setString(1, province);
            stm.setString(2, patientId);

            try (ResultSet rs = stm.executeQuery()) {

                List<Medic> allMedicAvailable = new ArrayList<>();

                int count = 0;
                while (rs.next()) {

                    Medic medic = new Medic();
                    medic.setUserName(rs.getString("username"));
                    medic.setName(rs.getString("name"));
                    medic.setSurname(rs.getString("surname"));
                    medic.setAddress(rs.getString("address"));
                    medic.setCity(rs.getString("city"));
                    allMedicAvailable.add(medic);

                }
                return allMedicAvailable;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public void changeMedic(String medicId, String patientId) throws DAOException {
        try (PreparedStatement stm = CON.prepareStatement(CHANGE_MEDIC)) {
            stm.setString(1, medicId);
            stm.setString(2, patientId);
            stm.executeUpdate();

        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public List<Visit> getAllVisit(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_ALL_VISIT)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                List<Visit> allVisit = new ArrayList<>();

                while (rs.next()) {

                    Visit visit = new Visit();
                    visit.setId(rs.getInt("id"));
                    visit.setDateTime(rs.getTimestamp("date_time"));
                    visit.setDescription(rs.getString("description"));
                    allVisit.add(visit);

                }
                return allVisit;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public List<SpecialisticVisitAppointment> getAllSpecialisticVisit(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_ALL_SPECIALISTIC_VISIT)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                List<SpecialisticVisitAppointment> allVisit = new ArrayList<>();

                while (rs.next()) {
                    SpecialisticVisitAppointment specialisticVisit = new SpecialisticVisitAppointment();
                    specialisticVisit.setId(rs.getInt("id"));
                    specialisticVisit.setName(rs.getString("name"));
                    specialisticVisit.setDateTime(rs.getTimestamp("date_time_appointment"));
                    specialisticVisit.setAnamnesis(rs.getString("anamnesis"));
                    specialisticVisit.setPayed(rs.getBoolean("payed"));
                    allVisit.add(specialisticVisit);

                }
                return allVisit;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public List<ExamPrescribedAtSSP> getAllExam(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_ALL_EXAM)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                List<ExamPrescribedAtSSP> allExam = new ArrayList<>();

                while (rs.next()) {
                    ExamPrescribedAtSSP exam = new ExamPrescribedAtSSP();
                    exam.setMade(rs.getBoolean("made"));
                    exam.setExamDate(rs.getTimestamp("exam_date"));
                    exam.setName(rs.getString("name"));
                    
                    allExam.add(exam);

                }
                return allExam;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public List<MedicinePrescription> getAllMedicine(String userName) throws DAOException {
        if (userName == null) {
            throw new DAOException("userName is mandatory fields", new NullPointerException("userName is null"));
        }

        try (PreparedStatement stm = CON.prepareStatement(GET_ALL_MEDICINE)) {
            stm.setString(1, userName);

            try (ResultSet rs = stm.executeQuery()) {

                List<MedicinePrescription> allMedicine = new ArrayList<>();

                while (rs.next()) {
                    MedicinePrescription medicine = new MedicinePrescription();
                    medicine.setId(rs.getInt("id"));
                    medicine.setName(rs.getString("name"));
                    medicine.setBrand(rs.getString("brand"));
                    medicine.setQuantity(rs.getInt("quantity"));
                    medicine.setDateTime(rs.getDate("date_time"));

                    allMedicine.add(medicine);

                }
                return allMedicine;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
    public Visit getSingleVisit(String visitId) throws DAOException {
        if (visitId == null) {
            throw new DAOException("visit id is mandatory fields", new NullPointerException("visitid is null"));
        }
        try (PreparedStatement stm = CON.prepareStatement(GET_SINGLE_VISIT)) {
            stm.setString(1, visitId);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                       Visit myVisit = new Visit();
                    myVisit.setDescription(rs.getString("description"));
                    return myVisit;

                }
                return null;
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }
    
    @Override
     public SpecialisticVisitAppointment getSpecialisticSingleVisit (String specialisticVisitId) throws DAOException {
        if (specialisticVisitId == null) {
            throw new DAOException("visit id is mandatory fields", new NullPointerException("visitid is null"));
        }
        try (PreparedStatement stm = CON.prepareStatement(GET_SINGLE_SPECIALISTIC_VISIT)) {
            System.out.println("visitId in servlet " + specialisticVisitId);
            stm.setString(1, specialisticVisitId);


            try (ResultSet rs = stm.executeQuery()) {

             

                while (rs.next()) {
                    SpecialisticVisitAppointment mySpecialisticVisit = new SpecialisticVisitAppointment();
                    
                    mySpecialisticVisit.setAnamnesis(rs.getString("anamnesis"));
                    return mySpecialisticVisit;

                }
                return null;
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
     }
}
