/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao.jdbc;

import com.mycompany.persistence.entities.SSP;
import com.mycompany.persistence.entities.dao.factories.jdbc.JDBCDAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.mycompany.persistence.dao.SSPDAO;
import java.sql.Statement;

/**
 *
 * @author aless
 */
public class JDBCSSPDAO extends JDBCDAO<SSP, String> implements SSPDAO {

    public JDBCSSPDAO(Connection con) {
        super(con);
    }

    final private String GET_ALL_PATIENTS_WITH_VISITS_TODAY = "SELECT p.name, p.surname, p.fiscal_code, p.email, exssp.name as examName, DATE_FORMAT(exam_date,'%H:%i:%s') time, essp.id as idAppointment, essp.made\n"
            + "FROM health_services_system.exam_prescribed_at_issp essp\n"
            + "JOIN visit v ON essp.visit_id = v.id\n"
            + "JOIN patient p ON v.id_patient = p.user_idUser\n"
            + "JOIN user up ON p.user_idUser = username\n"
            + "JOIN exam_ssp exssp ON essp.id_exam = exssp.id\n"
            + "WHERE DATE(exam_date) = ? AND up.province = ?;";

    final private String UPDATE_MADE_IN_EXAM_APPOINTMENT = "UPDATE `health_services_system`.`exam_prescribed_at_issp` SET `made` = '1' WHERE (`id` = ?);";

    final private String GET_DATA_FOR_REPORT_MEDICINE = "SELECT m.name as medicineName, mp.quantity as quantity, md.name as patientName, md.surname as patientSurname, p.name as medicName, p.surname as medicSurname\n"
            + "FROM medicine_prescription mp\n"
            + "JOIN medicine m ON m.id = mp.medicine_idmedicine\n"
            + "JOIN visit v ON v.id = mp.id_visit\n"
            + "JOIN patient p ON p.user_idUser = v.id_patient\n"
            + "JOIN medic md ON p.medic_id = md.user_idUser\n"
            + "JOIN user u ON u.username = p.user_idUser\n"
            + "WHERE u.province = ?;";

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SSP getByPrimaryKey(String arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SSP> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<List<String>> getAllPatientsWithVisitsToday(String province) throws DAOException {
        List<List<String>> patientsWithVisits = new ArrayList<>();
        try ( PreparedStatement stm = CON.prepareStatement(GET_ALL_PATIENTS_WITH_VISITS_TODAY)) {
            Date todayDate = new Date(System.currentTimeMillis());
            System.out.println("TODAY'S DATE: " + todayDate.toString());
            stm.setDate(1, todayDate);
            stm.setString(2, province);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    List<String> paramsInRow = new ArrayList<>();
                    paramsInRow.add(rs.getString("name"));
                    paramsInRow.add(rs.getString("surname"));
                    paramsInRow.add(rs.getString("fiscal_code"));
                    paramsInRow.add(rs.getString("email"));
                    paramsInRow.add(rs.getString("examName"));
                    paramsInRow.add(rs.getString("time"));
                    paramsInRow.add(Integer.toString(rs.getInt("idAppointment")));
                    paramsInRow.add(Boolean.toString(rs.getBoolean("made")));
                    patientsWithVisits.add(paramsInRow);
                }
                return patientsWithVisits;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public void updateMadeInExamAppointment(int idAppointment) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(UPDATE_MADE_IN_EXAM_APPOINTMENT)) {

            stm.setInt(1, idAppointment);
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

    @Override
    public List<List<String>> dataForReportMedicine(String province) throws DAOException {
        List<List<String>> rows = new ArrayList<>();
        try ( PreparedStatement stm = CON.prepareStatement(GET_DATA_FOR_REPORT_MEDICINE)) {
            stm.setString(1, province);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    List<String> paramsInRow = new ArrayList<>();
                    paramsInRow.add(rs.getString("medicineName"));
                    paramsInRow.add(rs.getString("quantity"));
                    paramsInRow.add(rs.getString("patientName"));
                    paramsInRow.add(rs.getString("patientSurname"));
                    paramsInRow.add(rs.getString("medicName"));
                    paramsInRow.add(rs.getString("medicSurname"));
                    rows.add(paramsInRow);
                }
                return rows;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }
    }

}
