/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao;

import com.mycompany.persistence.entities.User;
import com.mycompany.persistence.entities.dao.factory.DAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;

/**
 *
 * @author aless
 */
public interface UserDAO extends DAO<User, String>{
    public User getByIdAndPassword(String userName, String password) throws DAOException;
    public void updatePasswordByUsername(String username, String password, String salt) throws DAOException;
}
