/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.persistence.dao;

import com.mycompany.persistence.entities.SSP;
import com.mycompany.persistence.entities.dao.factory.DAO;
import com.mycompany.persistence.entities.dao.factory.DAOException;
import java.util.List;

/**
 *
 * @author aless
 */
public interface SSPDAO extends DAO<SSP, String> {
    public List<List<String>> getAllPatientsWithVisitsToday(String province) throws DAOException;
    public void updateMadeInExamAppointment(int appointmentId) throws DAOException;
    public List<List<String>> dataForReportMedicine(String province) throws DAOException;
}
