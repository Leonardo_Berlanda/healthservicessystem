/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function createExamsCards(jsonVisit, patientUserName) {
    $("#dateVisit").text("Data: " + jsonVisit.date_time);
    $("#descriptionVisit").text(jsonVisit.description);

    var htmlCardsString = "<div id='cardContainer' class='row'>";
    if (jsonVisit.sepcialisticVisitsAppointments != null) {
        var examCounter = 0;
        jsonVisit.sepcialisticVisitsAppointments.forEach(specialVisit => {
            if (specialVisit.anamnesis == null) {
                specialVisit.anamnesis = "Anamnesi non ancora compilata";
            }
            var payed = specialVisit.payed == 1 ? "SI" : "NO";
            var buttonPay = '<button id="payedButton-' + specialVisit.id + '" type="button" onclick="payVisit(' + specialVisit.id + ')" class="mx-2 btn btn-success">Paga visita</button>';
            if(payed == "SI"){
                buttonPay = '<button class="mx-2 btn btn-success" disabled>Pagato</button>'
            }
            var idExamCard = "exam-" + examCounter;
            htmlCardsString += "<div class='col-sm-12 mt-4'>\n\
                <div class='card' id='" + idExamCard + "'>\n\
                    <div class='card-body'>\n\
                        <h5 class='card-title text-uppercase'>" + specialVisit.specialisticVisitName + "</h5>\n\
                        <p class='card-text'>" + specialVisit.anamnesis + "</p>\n\
                        <ul class='list-group list-group-flush'>";
            htmlCardsString += "<li class='list-group-item'><div class='row'>\n\
                        " + buttonPay + "\n\
                        <a class='btn btn-primary' href='SchedaVisitaSpecialistica?specialisticVisitId=" + specialVisit.id + "&&patientId=" + patientUserName + "' role='button'>Effettua visita</a>\n\
                        </div></li>";
            htmlCardsString += "</ul></div>\n\
                </div>\n\
            </div>";
            examCounter++;
        });
    }
    if (jsonVisit.medicines != null) {
        jsonVisit.medicines.forEach(medicine => {
            htmlCardsString += "<div class='col-sm-4 my-4'>\n\
                <div class='card'>\n\
                    <div class='card-body'>\n\
                        <h5 class='card-title text-uppercase'>" + medicine.medicineName + "</h5>\n\
                        <p class='card-text'>Quantità: " + medicine.quantity + "</p>"
            htmlCardsString += "</div>\n\
                </div>\n\
            </div>";
        });
    }

    if (jsonVisit.examsAppointments != null) {
        jsonVisit.examsAppointments.forEach(exam => {
            var eseguito = exam.made === "1" ? "SI" : "NO"
            htmlCardsString += "<div class='col-sm-4 my-4'>\n\
                <div class='card'>\n\
                    <div class='card-body'>\n\
                        <h5 class='card-title text-uppercase'>" + exam.examName + "</h5>\n\
                        <p class='card-text'>Data: " + exam.date + "</p>\n\
                        <p class='card-text'>Eseguito: " + eseguito + "</p>"
            htmlCardsString += "</div>\n\
                </div>\n\
            </div>";
        });
    }


    htmlCardsString += "</div>";

    $("#content").append(htmlCardsString);
}

function payVisit(idVisit){
    var loadingButton = '<div class="mx-2" id="payedButton-' + idVisit + '">'
            + '<button class="btn btn-primary" type="button" disabled>'
            + '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'
            + ' Modificando...'
            + '</button>'
            + '</div>';
    
    
    $("#payedButton-" + idVisit).replaceWith(loadingButton);
    $.post("/health_services_system/payVisit", {"idVisit" : idVisit}, function (data){
        $("#payedButton-" + idVisit).replaceWith('<button class="mx-2 btn btn-success" disabled>Pagato</button>');
    });
}
