/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var specialisticVisitCardCounter = 0;
var medicineCardCounter = 0;
var examsSSPCounter = 0;
function addSpecialisticVisit() {
    var oldCard = "";
    oldCard += "\
                        <div class='card' id='specialisticVisit-" + specialisticVisitCardCounter + "'>\n\
                            <div class='row'>\n\
                                <h5 class='card-title m-1 col-sm-12'>Visita specialistica</h5>\n\
                            </div>\n\
                            <div class='card-body row' id='specialisticVisit-" + specialisticVisitCardCounter + "'>\n\
                                <div class='it - datepicker-wrapper'>\n\
                                  <div class='form-group'>\n\
                                    <input id='date-picker-" + specialisticVisitCardCounter + "' class='date-picker-" + specialisticVisitCardCounter + " form-control it-date-datepicker' type='text' placeholder='Appuntamento'>\n\
                                    </div>\n\
                                </div>\n\
                                <div>\n\
                                    <input id='time-picker' type='text' name='timepicker' class='timepicker col-sm-12 form-control mb-3'/>\n\
                                </div>\n\
                                <div class='input-group mb-3'>\n\
                                    <input type='text' class='tags-specialisticVisits ui-widget form-control specialisticVisit' placeholder='Visita specialistica' aria-label='Visita specialistica' aria-describedby='basic-addon1'>\n\
                                </div>\n\
                                <div class=''>\n\
                                    <button type='button' class='btn btn-danger' onclick='deleteCard(\"specialisticVisit-" + specialisticVisitCardCounter + "\")'>Cancella</button>\n\
                                </div>\n\
                            </div>\n\
                        </div>";
    $("#cardButtons").replaceWith(oldCard);
    var newCard = createNewCard();
    $("#cardSpecialVisitContainer").append(newCard);
    $('.date-picker-' + specialisticVisitCardCounter).datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $('.timepicker').wickedpicker({twentyFour: true});
    specialisticVisitCardCounter++;

    $(".tags-specialisticVisits").click(function () {
        var subString = "";
        $.get("getSpecialisticVisitsBySubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(visita => {
                tags.push(visita.name);
            });
            console.log("tags length: " + tags.length);
            $(".tags-specialisticVisits").autocomplete({
                source: tags,
                minLength: 0
            }).focus(function () {
                $(this).autocomplete('search', $(this).val())
            });
        });
    });

    $(".tags-specialisticVisits").keyup(function () {
        var subString = $(this).val();
        $.get("getSpecialisticVisitsBySubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(visita => {
                tags.push(visita.name)
            })
            $(".tags-specialisticVisits").autocomplete({
                source: tags
            }).focus(function () {
                $(this).autocomplete('search', $(this).val())
            });
        });
    });
}

function addMedicinePrescription() {
    var oldCard = "\
                        <div class='card' id='medicine-" + medicineCardCounter + "'>\n\
                            <div class='row'>\n\
                                <h5 class='card-title m-1 col-sm-12'>Prescrizione farmaco</h5>\n\
                            </div>\n\
                            <div class='card-body row' id='medicine-" + medicineCardCounter + "'>\n\
                                <div class='input-group mb-3' ui-widget>\n\
                                    <input type='text' class='tags-medicine form-control' placeholder='Farmaco' aria-label='Farmaco' aria-describedby='basic-addon1'>\n\
                                </div>\n\
                                <input id='number-picker-" + medicineCardCounter + "' type='number' value='1' min='1' max='20' step='1'/>\n\
                            </div>\n\
                            <div class=''>\n\
                                <button type='button' class='btn btn-danger' onclick='deleteCard(\"medicine-" + medicineCardCounter + "\")'>Cancella</button>\n\
                            </div>\n\
                        </div>";
    $("#cardButtons").replaceWith(oldCard);
    var newCard = createNewCard();
    $("#cardSpecialVisitContainer").append(newCard);

    $(".tags-medicine").click(function () {
        var subString = "";
        $.get("getMedicinesByStartSubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(visita => {
                tags.push(visita.name);
            });
            $(".tags-medicine").autocomplete({
                source: tags,
                minLength: 0
            }).focus(function () {
                $(this).autocomplete('search', $(this).val())
            });
        });
    });

    $(".tags-medicine").keyup(function () {
        var subString = $(this).val();
        $.get("getMedicinesByStartSubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(medicine => {
                tags.push(medicine.name);
            });
            $(".tags-medicine").autocomplete({
                source: tags
            });
        });
    });
    $("#number-picker-" + medicineCardCounter).inputSpinner();
    medicineCardCounter++;
}

function addExamSSP() {
    var oldCard = "";
    oldCard += "\
                        <div class='card' id='exam-" + examsSSPCounter + "'>\n\
                            <div class='row'>\n\
                                <h5 class='card-title m-1 col-sm-12'>Esame SSP</h5>\n\
                            </div>\n\
                            <div class='card-body row' id='exam-" + examsSSPCounter + "'>\n\
                                <div class='it - datepicker-wrapper'>\n\
                                  <div class='form-group'>\n\
                                    <input id='date-pickerSSP-" + examsSSPCounter + "' class='date-pickerSSP-" + examsSSPCounter + " form-control it-date-datepicker' type='text' placeholder='Appuntamento'>\n\
                                    </div>\n\
                                </div>\n\
                                <div>\n\
                                    <input id='time-picker' type='text' name='timepicker' class='timepicker col-sm-12 form-control mb-3'/>\n\
                                </div>\n\
                                <div class='input-group mb-3'>\n\
                                    <input type='text' class='tags-examsSSP ui-widget form-control examSSP' placeholder='Esame' aria-label='Esame' aria-describedby='basic-addon1'>\n\
                                </div>\n\
                            </div>\n\
                            <div class=''>\n\
                                <button type='button' class='btn btn-danger' onclick='deleteCard(\"exam-" + examsSSPCounter + "\")'>Cancella</button>\n\
                            </div>\n\
                        </div>";
    $("#cardButtons").replaceWith(oldCard);
    var newCard = createNewCard();
    $("#cardSpecialVisitContainer").append(newCard);
    $('.date-pickerSSP-' + examsSSPCounter).datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $('.timepicker').wickedpicker({twentyFour: true});
    examsSSPCounter++;

    $(".tags-examsSSP").click(function () {
        var subString = $(this).val();;
        $.get("getExamsSSPBySubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(visita => {
                tags.push(visita.name);
            });
            $(".tags-examsSSP").autocomplete({
                source: tags,
                minLength: 0
            }).focus(function () {
                $(this).autocomplete('search', $(this).val())
            });

        });
    });

    $(".tags-examsSSP").keyup(function () {
        var subString = $(this).val();
        $.get("getExamsSSPBySubString", {subString: subString}, function (data) {
            var tags = [];
            data.forEach(visita => {
                tags.push(visita.name)
            })
            $(".tags-examsSSP").autocomplete({
                source: tags
            });
        });
    });
}


function createNewCard() {
    var newCard = "\n\
                    <div class='col-lg-4 my-4'>\n\
                        <div class='card' id='cardButtons'>\n\
                            <div class='row'>\n\
                                <h5 class='card-title m-1 col-sm-12'>Aggiungi</h5>\n\
                            </div>\n\
                            <div class='card-body' id='addCard'>\n\
                                <div class='form-group row'>\n\
                                    <button type='button' onclick='addSpecialisticVisit()' class='btn btn-outline-secondary my-2 col-sm-12 text-truncate'>Visita specialistica\n\
                                    </button>\n\
                                    <button type='button' onclick='addMedicinePrescription()' class='btn btn-outline-secondary my-2 col-sm-12 text-truncate'>Prescrizione farmaco \n\
                                    </button>\n\
                                     <button type='button' onclick='addExamSSP()' class='btn btn-outline-secondary my-2 col-sm-12 text-truncate'>Esame SSP \n\
                                    </button>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>";
    return newCard;
}


function deleteCard(id) {
    $("#" + id).parent().remove();
}


function saveVisit(idPatient) {
    console.log("SAVE VISIT");
    $("#saveVisitButton").replaceWith('<div id="buttonContainer">'
            + '<button class="btn btn-primary" type="button" disabled>'
            + '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'
            + ' Salvando...'
            + '</button>'
            + '</div>')

    var anamnesys = $("#anamnesysTextArea").val();
    var medicines = [];
    for (var i = 0; i < medicineCardCounter; i++) {
        if ($("#medicine-" + i).find(".form-control").val() != undefined) {
            var medicine = $("#medicine-" + i).find(".form-control").val();
            var quantity = $("#number-picker-" + i).val();
            if (medicine != "") {
                medicines.push({
                    "medicineName": medicine,
                    "quantity": quantity
                });
            }
        }

    }
    medicines = JSON.stringify(medicines);
    var specialisticVisitAppointments = [];
    for (var i = 0; i < specialisticVisitCardCounter; i++) {
        if ($("#specialisticVisit-" + i).find(".it-date-datepicker").val() != undefined) {
            var appointment = $("#specialisticVisit-" + i).find(".it-date-datepicker").val();
            var specialisticVisitName = $("#specialisticVisit-" + i).find(".specialisticVisit").val();
            var time = $("#specialisticVisit-" + i).find("#time-picker").val();
            appointment += " " + time;
            specialisticVisitAppointments.push({
                "appointment": appointment,
                "specialisticVisitName": specialisticVisitName
            });
        }
    }

    specialisticVisitAppointments = JSON.stringify(specialisticVisitAppointments);
    var examSSPAppointments = [];
    for (var i = 0; i < examsSSPCounter; i++) {
        if (appointment = $("#exam-" + i).find(".it-date-datepicker").val() != undefined) {
            var appointment = $("#exam-" + i).find(".it-date-datepicker").val();
            var examName = $("#exam-" + i).find(".examSSP").val();
            var time = $("#exam-" + i).find("#time-picker").val();
            appointment += " " + time;
            examSSPAppointments.push({
                "appointment": appointment,
                "examName": examName
            });
        }
    }

    examSSPAppointments = JSON.stringify(examSSPAppointments);
    var visitJson = {"idPatient": idPatient,
        "anamnesys": anamnesys,
        "medicines": medicines,
        "specialisticVisitAppointments": specialisticVisitAppointments,
        "examSSPAppointments": examSSPAppointments
    };
    console.log("VISIT JSON: " + JSON.stringify(visitJson));
    $.post("/health_services_system/MedicServlet", visitJson, function (data) {
        var patientId = window.location.search.split("=")[1];
        window.location.assign("/health_services_system/MedicServlet/SchedaPaziente?patientId=" + patientId);
    });
}