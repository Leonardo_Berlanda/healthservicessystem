/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function init() {

    var dataSrc = [];
    $('#patientsWithExamsToday-datatable').DataTable({
        "language": {
            "emptyTable": "Nessun esame prescritto per oggi"
        },
        "order": [[0, "desc"]],
        "columnDefs": [
            {"width": "20%", "targets": 0}
        ],
        'initComplete': function () {
            var api = this.api();
            // Populate a dataset for autocomplete functionality
            // using data from first, second and third columns
            api.cells('tr', [0, 1, 3]).every(function () {
                // Get cell data as plain text
                var data = $('<div>').html(this.data()).text();
                if (dataSrc.indexOf(data) === -1) {
                    dataSrc.push(data);
                }
            });
            // Sort dataset alphabetically
            dataSrc.sort();
            // Initialize Typeahead plug-in
            $('.dataTables_filter input[type="search"]', api.table().container())
                    .typeahead({
                        source: dataSrc,
                        afterSelect: function (value) {
                            api.search(value).draw();
                        }
                    });
        }
    });
}

function todo() {
    console.log("Feature yet to do");
}


function doExam(idAppointment) {
    var loadingButton = '<div id="buttonContainer-' + idAppointment + '">'
            + '<button class="btn btn-primary" type="button" disabled>'
            + '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'
            + ' Modificando...'
            + '</button>'
            + '</div>';
    $("#buttonContainer-" + idAppointment).replaceWith(loadingButton)
    $.ajax({
        url: '/health_services_system/doExam',
        type: 'PUT',
        success: function (result) {
            var succededTick = '<div id="buttonContainer-' + idAppointment + '"><button class="btn btn-success" disabled>Eseguito</button><div>';
            $("#buttonContainer-" + idAppointment).replaceWith(succededTick)
        },
        data: {"idAppointment": idAppointment}
    });
}


function downloadReport(province) {
    $.get("/health_services_system/getReport", {"province": province}, function (data) {

    });
}