/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function createTable(jsonVisits, patientId) {
    $("#dropdownMenuButton").text("Visite");
    var counter = 0;
    var htmlTableString = "<div id='content' class='col-lg-9 mt-2'>\n\
                   <p class='lead text-truncate'>Storico visite:</p>\n\
                 <table id='visitsTable' class='table table-bordered table-striped text-left table-hover'>\n\
                <thead><tr>\n\
                    <th scope='col'>Data visita</th>\n\
                    <th scope='col'>Descrizione</th>\n\
                    <th scope='col'>Prescrizioni</th>\n\
                    <th scope='col'>Visite</th>\n\
                    <th scope='col'>Esami</th>\n\
                </tr></thead><tbody>";
    jsonVisits.forEach(visit => {
        var visitId = visit.visit_id;
        var medicinesLenght = "";
        if (visit.medicines != null) {
            medicinesLenght += visit.medicines.length + " ";
        } else {
            medicinesLenght += "0 ";
        }

        var specialVisitsLenght = "";
        if (visit.sepcialisticVisitsAppointments != null) {
            specialVisitsLenght += visit.sepcialisticVisitsAppointments.length + " ";
        } else {
            specialVisitsLenght = "0 ";
        }
        
        var examVisitsLength = "";
        if (visit.examsAppointments != null) {
            examVisitsLength += visit.examsAppointments.length + " ";
        } else {
            examVisitsLength = "0 ";
        }
        
        var data_href = "SchedaVisita?patientId=" + patientId + "&&visitId=" + visitId;
        htmlTableString += "\n\
        <tr class='clickable-row' data-href='" + data_href + "' style='cursor: pointer;'>\n\
            <td> <div class='cell_of_table' style='height:20px; overflow:hidden;'>" + visit.date_time + "</div></td>\n\
            <td> <div class='cell_of_table' style='height:20px; overflow:hidden;'>" + visit.description + "</div></td>\n\
            <td> <div class='cell_of_table'>" + medicinesLenght + "medicine" + "</div></td>\n\
            <td> <div class='cell_of_table'>" + specialVisitsLenght + "visite" + "</div></td>\n\
            <td> <div class='cell_of_table'>" + examVisitsLength + "esami" + "</div></td>\n\
        </tr>";
        counter++;
    });
    htmlTableString += "</tbody></table> </div>";
    $("#content").replaceWith(htmlTableString);
    $(".clickable-row").click(function () {
        console.log("CLICKED");
        window.location.assign($(this).data("href"));
    });
    var dataSrc = [];
    $('#visitsTable').DataTable({
        "order": [[0, "desc"]],
        "columnDefs": [
            {"width": "20%", "targets": 0}
        ],
        'initComplete': function () {


            var api = this.api();

            // Populate a dataset for autocomplete functionality
            // using data from first, second and third columns
            api.cells('tr', [0, 1]).every(function () {
                // Get cell data as plain text
                var data = $('<div>').html(this.data()).text();
                if (dataSrc.indexOf(data) === -1) {
                    dataSrc.push(data);
                }
            });

            // Sort dataset alphabetically
            dataSrc.sort();

            // Initialize Typeahead plug-in
            $('.dataTables_filter input[type="search"]', api.table().container())
                    .typeahead({
                        source: dataSrc,
                        afterSelect: function (value) {
                            api.search(value).draw();
                        }
                    });
        }
    });
}

var jsonVisits;
var jsonVisitsWithExams = [];


function setJsonVisits(jsonVisitsString) {
    jsonVisits = jsonVisitsString;
    jsonVisits.forEach(visit => {
        if (visit.sepcialisticVisitsAppointments != null) {
            jsonVisitsWithExams.push(visit);
        }
    })
}

function createExamsCards(patientId, pageNumber) {

    var lastExamInPage = pageNumber * 5;
    var firstExamInPage = lastExamInPage - 4;
    var btnPreviousDisabled = false;
    var btnNextDisabled = false;
    if (lastExamInPage - jsonVisitsWithExams.length >= 0) {
        btnNextDisabled = true;
    }
    if (pageNumber === 1) {
        btnPreviousDisabled = true;
    }

    var indexToIterate = lastExamInPage;
    if (btnNextDisabled) {
        indexToIterate = jsonVisitsWithExams.length;
    }


    $("#dropdownMenuButton").text("Esami");
    var htmlCardsString = "<div id='content' class='col-lg-9'>\n\
                            <p class='lead text-truncate'>Storico esami:</p>\n\
                            \n\ <div id='cardContainer' class='row'>";

    for (var i = firstExamInPage - 1; i < indexToIterate; i++) {
        var visit = jsonVisitsWithExams[i];
        var visitId = visit.visit_id;
        if (visit.sepcialisticVisitsAppointments != null) {
            htmlCardsString += "<div class='col-sm-12 mt-4'>\n\
                <div class='card'>\n\
                    <div class='card-body'>\n\
                        <h5 class='card-title'>Visita del " + visit.date_time + "</h5>\n\
                        <ul class='list-group list-group-flush'>";

            var examCounter = 0;
            visit.sepcialisticVisitsAppointments.forEach(specialVisit => {
                var data_href = "SchedaVisita?patientId=" + patientId + "&&visitId=" + visitId + "&&examIndex=" + examCounter;
                htmlCardsString += "<li class='list-group-item'><div class='row'>\n\
                        <p class='col-sm-4' style='float: left; padding-right:20px'>Esame: " + specialVisit.specialisticVisitName + "</p>\n\
                        <p class='col-sm-4' style='float: left; padding-right:20px'> Appuntamento: " + specialVisit.date + "</p>\n\
                        <a href='" + data_href + "' class='btn btn-primary stretched-link col-sm-2'>Dettagli</a>\n\
                        </div></li>";
                examCounter++;
            });
            htmlCardsString += "</ul></div>\n\
                </div>\n\
            </div>";
        }
    }
    var startButtonDivs = "<div class='col-lg-9 fixed-bottom'>";
    var endButtonDivs = "</div>";
    var buttonPrevious = "<button type='button' onclick='createExamsCards(\"" + patientId + "\"," + (pageNumber - 1) + ")' class='btn btn-primary m-3'>Precedente</button>";
    var buttonNext = "<button type='button' onclick='createExamsCards(\"" + patientId + "\"," + (pageNumber + 1) + ")' class='btn btn-primary m-3'>Prossimo</button>";
    if (btnPreviousDisabled) {
        buttonPrevious = '<button type="button" class="btn btn-primary m-3" disabled>Precedente</button>';
    }
    if (btnNextDisabled) {
        buttonNext = '<button type="button" class="btn btn-primary m-3" disabled>Prossimo</button>';
    }
    var buttons = startButtonDivs + buttonPrevious + buttonNext + endButtonDivs;
    htmlCardsString += buttons + "</div> </div>";

    $("#content").replaceWith(htmlCardsString);
}

