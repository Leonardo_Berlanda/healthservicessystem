<%-- 
    Document   : indexjsp
    Created on : 2 feb 2020, 11:39:35
    Author     : aless
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Authentication Area</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel="stylesheet" href="css/bootstrap-italia.min.css">
        <script src="js/bootstrap-italia.bundle.min.js"></script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/newstyle.scss">
        <link rel="stylesheet" href="css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300' rel='stylesheet' type='text/css'>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">





        <!-- Required Meta Tags -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">


        <!-- Favicon -->
        <link rel="shortcut icon" href="images/logo/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="css/animate-3.7.0.css">
        <link rel="stylesheet" href="css/font-awesome-4.7.0.min.css">
        <link rel="stylesheet" href="css/owl-carousel.min.css">
        <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/style.css">

        <style>
            body{
                background-color: white
            }
            #loginform {

                position: absolute;
                top: 50%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%) ;

            }

            #userType{
                padding-bottom: 10px;
                padding-left: 30%;
                width: 100%;
            }
        </style>
    </head>

    <body>
        <div class="cookiebar">
            <p>Questo sito utilizza cookie tecnici, analytics e di terze parti. <br>Proseguendo nella navigazione accetti l’utilizzo dei cookie.</p>
            <div class="cookiebar-buttons">
                <a href="https://designers.italia.it/privacy-policy/" class="cookiebar-btn">Informativa<span class="sr-only">cookies</span></a>
                <button data-accept="cookiebar" class="cookiebar-btn cookiebar-confirm">Accetto<span class="sr-only"> i cookies</span></button>
            </div>
        </div>
        <!--<div id="loader-wrapper">
             <div id="loader"></div>
            <div class="loader-section section-left">
                 <img style="max-width: 50px"  src="images/logo/heart.svg">
             </div>
             <div class="loader-section section-right"></div>
         </div> -->
        <div id="content">
            <header class="header-area">
                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-9 d-md-flex">
                                <h6 class="mr-3"><span class="mr-2"><i class="fa fa-mobile"></i></span> Numero Centralino +39 02 0000 0000</h6>
                                <h6 class="mr-3"><span class="mr-2"><i class="fa fa-envelope-o"></i></span> info@ministerodellasalute.com</h6>
                                <h6><span class="mr-2"><i class="fa fa-map-marker"></i></span>Viale Giorgio Ribotta, 5 00144 - Roma</h6>
                            </div>
                            <div class="col-lg-3">
                                <div class="social-links">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div id="header" id="home">
                    <div class="container">
                        <div class="row align-items-center justify-content-between d-flex">
                            <div id="logo">
                                <a href="index.jsp"><img src="images/logo/logo.png" alt="" title="" /></a>
                            </div>

                            <nav id="nav-menu-container">
                                <ul class="nav-menu">      				          
                                </ul>
                            </nav><!-- #nav-menu-container -->		    		
                        </div>
                    </div>
                </div>
            </header>
            <section class="banner-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <figure class="figure">
                                <img src="images/ministero-della-salute.png" class="figure-img img-fluid rounded" alt="...">
                            </figure>
                            <div id="mainButton">
                                <div class="btn-text" onclick="openForm()">Accedi</div>
                                <div class="modal">
                                    <div class="close-button" onclick="closeForm()">x</div> 

                                    <form id="loginform" class="form-signin" action="login.handler" method="POST">


                                        <div class="bootstrap-select-wrapper">
                                            <select name="userType">
                                                <option value="patient">Paziente</option>
                                                <option value="medic">Medico</option>
                                                <option value="ssp">SSP</option>
                                            </select>
                                        </div>

                                        <div class="input-group">
                                            <input type="text" id="name" onblur="checkInput(this)" id="username" name="username" class="form-control"  required autofocus>
                                            <label for="name">Username</label>
                                        </div>
                                        <div class="input-group">
                                            <input type="password" id="password" onblur="checkInput(this)" id="password" name="password" class="form-control"  required>
                                            <label for="password">Password</label>
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" name="ricordami" value="True">Ricordami<br>
                                        </div>
                                        <button class="form-button" type="submit">Accedi</button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer-area section-padding">
                <div class="footer-widget">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl">
                                <div class="single-widget-home mb-5 mb-lg-0">
                                    <h3 class="mb-4">Partita IVA: IT 11359591002</h3>
                                </div>
                            </div>
                            <div class="col-xl">
                                <div class="single-widget-home mb-5 mb-lg-0">
                                    <img src="images/ministero-della-salute-small.png">
                                </div>
                            </div>
                            <div class="col-xl">
                                <div class="single-widget-home mb-5 mb-lg-0">
                                    <h3 class="mb-4">Viale Giorgio Ribotta, 5 00144 - Roma</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-md-6">
                                <span>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Leonardo Berlanda | Alessandro Grassi 
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </span>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="social-icons">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Javascript -->
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/vendor/bootstrap-4.1.3.min.js"></script>
        <script src="js/vendor/wow.min.js"></script>
        <script src="js/vendor/owl-carousel.min.js"></script>
        <script src="js/vendor/jquery.datetimepicker.full.min.js"></script>
        <script src="js/vendor/jquery.nice-select.min.js"></script>
        <script src="js/vendor/superfish.min.js"></script>
        <script src="js/main.js"></script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>




    </body>

</html>

