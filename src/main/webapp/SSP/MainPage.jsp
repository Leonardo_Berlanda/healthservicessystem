<%-- 
    Document   : MainPage
    Created on : 29 gen 2020, 11:18:24
    Author     : aless
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Bootstrap CSS -->

        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!--DATATABLE-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>



        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <script type="text/javascript" src="js/SSP.js"></script>
        <title>SSP</title>
    </head>
    <body>
        <script>
            $(document).ready(function () {
                init();
            });
        </script>
        <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light border-bottom">
            <div style="padding-right: 20px">
                <p class="lead">${ssp.getProvince()}</p>
            </div>

            <div>
                <a class="btn btn-primary" href="/health_services_system/getReport?province=${ssp.getProvince()}" role="button">Scarica resoconto</a>
            </div>

            <div>
                <button class="mx-2 btn btn-primary" data-toggle="modal" data-target="#changePasswordModal" >Cambia Password</button>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <a id="logouttt" class="btn btn-danger" role="button" href="/health_services_system/logout.handler">LogOut</a>
                </ul>
            </div>
        </nav>
        <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">CAMBIA PASSWORD</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-8">

                                <form style="min-width: 100%; background-color: white; " type="submit" action="changePassword" method="post">
                                    <input  name="username" class="invisible" value="${ssp.getUserName()}">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password Corrente</label>
                                        <input name="oldPassword" type="password" class="form-control" id="exampleInputPassword1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Nuova Password</label>
                                        <input  name="newPassword" id="password1" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Ripeti la Password</label>
                                        <input  id="password2" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                    </div>
                                    <p id="segnalPassword">Password vuota</p>
                                    <button id="buttonPassword"  disabled="true"  type="submit" class="btn btn-primary">Submit</button>

                                </form>

                            </div>  
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="patientsWithExamsToday">
            <div class='raw mt-2 mx-2' id='div_table'>
                <table id='patientsWithExamsToday-datatable' class='table table-bordered table-striped text-center table-responsive-md table-hover'>
                    <thead>
                        <tr>
                            <th scope='col'>Paziente</th>
                            <th scope='col'>Codice fiscale</th>
                            <th scope='col'>Email</th>
                            <th scope='col'>Esame da fare</th>
                            <th scope='col'>Ora</th>
                            <th scope='col'></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${allPatientsWithVisitToday}" var = "examAppointment">
                            <tr class='clickable-row'>
                                <td>${examAppointment.get(0)} ${examAppointment.get(1)}</td>
                                <td>${examAppointment.get(2)}</td>
                                <td>${examAppointment.get(3)}</td>
                                <td>${examAppointment.get(4)}</td>
                                <td>${examAppointment.get(5)}</td>
                                <c:choose>
                                    <c:when test = "${examAppointment.get(7).equals('false')}">
                                        <td><div id='buttonContainer-${examAppointment.get(6)}'><button type="button" id='button-${examAppointment.get(6)}' onclick="doExam(${examAppointment.get(6)})" class="btn btn-primary">Esegui visita</button></div></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><div id="buttonContainer-${examAppointment.get(6)}"><button class="btn btn-success" disabled>Eseguito</button></div></td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <script>$(".passwordForm[type=password]").keyup(function () {

                if ($("#password1").val() == '' || $("#password2").val() == '') {
                    $("#buttonPassword").attr("disabled", true);
                    $("#segnalPassword").text("Almeno un campo password vuoto");
                } else if ($("#password1").val() == $("#password2").val()) {
                    $("#buttonPassword").attr("disabled", false);
                    $("#segnalPassword").text("Ok");
                } else {
                    $("#buttonPassword").attr("disabled", true);
                    $("#segnalPassword").text("Le password non corrispondono");
                }
            });</script>
    </body>
</html>
