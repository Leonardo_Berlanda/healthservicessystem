<%-- 
    Document   : patient
    Created on : 29 ott 2019, 15:31:55
    Author     : Leona
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="it">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">        
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <!--JQUERY-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
        <!--DATATABLE-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
        <link rel="stylesheet"  type="text/css" href="css/patient.css">

        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        -->
        <title>Area personale</title>

    </head>
    <body>


        <!--########## NAVBAR ##########-->
        <div class="d-flex" id="wrapper">
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading"> 
                    <p class="navbar-brand" href="#">
                        <object data="images/profile/profile_picture_${dataPatients.getUserName()}.jpg" type="image/png"  width="100" height="100"  id="nav-photo" class="d-inline-block align-top" alt="">
                            <img src="images/unknownperson.png" alt="Stack Overflow logo and icons and such"  width="100" height="100"  id="nav-photo" class="d-inline-block align-top" alt="">
                        </object>
                        <br>
                        ${dataPatients.name} ${dataPatients.surname}
                    </p>
                </div>
                <div class="list-group list-group-flush">
                    <a href="#" id="cardchose" class="list-group-item list-group-item-action bg-light"  >Home</a>
                    <a href="#" id="medicChange" class="list-group-item list-group-item-action bg-light">Cambia Medico</a>
                    <a href="#" id="viewVisitAndExam" class="list-group-item list-group-item-action bg-light">Esami e visite</a>
                    <a href="#" id="viewMedicine" class="list-group-item list-group-item-action bg-light">Ricette e Prescrizioni</a>
                </div>
            </div>
            <div id="page-content-wrapper">
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Menu</button>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <button id="logouttt" class="btn btn-danger" data-toggle="modal" data-target="#logoutModal" >LogOut</button>
                        </ul>
                    </div>
                </nav>
                <div  class="container-fluid">



                    <!--######## LOGOUT ######## -->
                    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">LOGOUT</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Sicuro di volerti sloggare?</p>
                                </div>
                                <div class="modal-footer">
                                    <form  id="loginform" class="form-signin" action="logout.handler" method="POST"><button id="logouttt" type="submit" class="btn btn-danger" >Si sono sicuro</button></form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--######## VIEW ######## -->
                    <div id="viewModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">VISITA</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p></p>
                                </div>

                            </div>
                        </div>   

                    </div>
                    <!--########## PAGE CONTENT ##########-->
                    <!--########## HOME ##########-->
                    <div id="containerHome-hide">
                        <br>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="card mb-3" >
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <object data="images/profile/profile_picture_${dataPatients.getUserName()}.jpg" type="image/png" id="nav-photo" class="card-img">
                                                <img src="images/unknownperson.png" alt="Stack Overflow logo and icons and such" id="nav-photo" class="card-img">
                                            </object>
                                            <br>
                                            <p></p>
                                            <div class="card-body">
                                                <form action="updatePhoto.html"  method="post" enctype="multipart/form-data" class="center">                                                                                 
                                                    <p>Cambia foto</p>
                                                    <label class="custom-file-upload">
                                                        <i class="fa fa-cloud-upload"></i> 
                                                        <input type="file" name="file1" accept="image/*">
                                                        <input type="submit">

                                                    </label>        
                                                </form>                                    
                                            </div>
                                            <p></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">  ${dataPatients.name} ${dataPatients.surname}</h5>
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item">Data di nascita: ${dataPatients.birthDate}, ${dataPatients.birthLocation}</li>
                                                    <li class="list-group-item">Email: ${dataPatients.email}</li>
                                                    <li class="list-group-item">Sesso: ${dataPatients.sex}</li>
                                                    <li class="list-group-item">Codice fiscale: ${dataPatients.fiscalCode}</li>
                                                </ul>
                                            </div>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div id="card" class="card" style="width: 18rem;">
                                    <!-- <img src="..." class="card-img-top" alt="..."> -->
                                    <div class="card-body">
                                        <h5 class="card-title">Cambia Password</h5>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#changePasswordModal" >Cambia</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="card" class="card" style="width: 18rem;">
                            <!-- <img src="..." class="card-img-top" alt="..."> -->
                            <div class="card-body">
                                <h5 class="card-title">Proprio Medico</h5>
                                <p class="card-text"> ${dataPatientsMedic.name} ${dataPatientsMedic.surname}</p>
                                <p class="card-text">Indirizzo: ${dataPatientsMedic.address}, ${dataPatientsMedic.city}</p>
                            </div>
                        </div>

                    </div>

                    <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">CAMBIA PASSWORD</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-8">

                                            <form style="min-width: 100%; background-color: white; " type="submit" action="changePassword" method="post">
                                                <input  name="username" class="invisible" value="${dataPatients.userName}">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Password Corrente</label>
                                                    <input name="oldPassword" type="password" class="form-control" id="exampleInputPassword1">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Nuova Password</label>
                                                    <input  name="newPassword" id="password1" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Ripeti la Password</label>
                                                    <input  id="password2" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                                </div>
                                                <p id="segnalPassword">Password vuota</p>
                                                <button id="buttonPassword"  disabled="true"  type="submit" class="btn btn-primary">Submit</button>

                                            </form>

                                        </div>  
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <br>
                <!--########## CHANGE MEDIC ##########-->
                <div id="containerChangeMedic-hide">
                    <br>
                    <h5> IL TUO MEDICO ATTUALE </h5>
                    <br>
                    <div id="card" class="card" style="width: 18rem;">
                        <!-- <img src="..." class="card-img-top" alt="..."> -->
                        <div class="card-body">
                            <h5 class="card-title">${dataPatientsMedic.name} ${dataPatientsMedic.surname}</h5>
                            <p class="card-text">Indirizzo: </p>
                            <p>${dataPatientsMedic.address}, ${dataPatientsMedic.city}</p>

                        </div>
                    </div>
                    <br>
                    <br>
                    <h5>MEDICI DISPONIBILI NELLA TUA ZONA</h5>
                    <br>
                    <div class="card-deck">
                        <c:forEach var="medic" items="${AllmedicAvailable}" varStatus="loop">
                            <div id="card" class="card" style="width: 18rem;">
                                <!-- <img src="..." class="card-img-top" alt="..."> -->
                                <div class="card-body">
                                    <h5 class="card-title">${medic.name} ${medic.surname}</h5>
                                    <br>
                                    <p class="card-text">Indirizzo: </p>
                                    <p>${medic.address}, ${medic.city}</p>
                                    <form style="width: 0px" type="submit" action="change.medic" method="POST"><button type="submit" name="btnM" type="submit" class="btn btn-outline-primary" value="${loop.count}">Scegli</button></form>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <!--########## VIEW VISIT AND EXAM ##########-->
                <div id="containerViewVisitAndExam-hide">
                    <br>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#table1" aria-expanded="true" aria-controls="table1">VISITE</button>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#table2" aria-expanded="false" aria-controls="table2">VISITE SPECIALISTICHE</button>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#table3" aria-expanded="false" aria-controls="table3">ESAMI</button> 


                    <div id="table1" class="collapse table-responsive show">
                        <br>
                        <h5> VISITE </h5>
                        <br>
                        <table id="visitTable" class="table">
                            <caption>List of Visit</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">Informazioni</th>
                                    <th scope="col">Info</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="visit" items="${allVisit}" varStatus="loop">
                                    <tr>
                                        <th scope="row">${loop.count}</th>
                                        <td>${visit.dateTime}</td>
                                <style>
                                    .overflow {
                                        width: 10em;
                                        margin: 0 0 2em 0;

                                        /**
                                         * Required properties to achieve text-overflow
                                         */
                                        white-space: nowrap;
                                        overflow: hidden;
                                    }
                                </style>
                                <style>.ellipsis { text-overflow: ellipsis; }</style>
                                <td ><p class="overflow ellipsis">${visit.description}</p></td>
                               <!-- <td><a name="view_visit" value="${loop.count}" href="#" data-toggle="modal" data-target="#viewModal" >CONTROLLA</a></td> -->
                                <td>    
                                    <button  class="btn btn-outline-primary" onclick="controllTheVisit(${visit.id})">CONTROLLA</button>  <!--data-toggle="modal" data-target="#viewModal" --> 
                                </td>
                            </c:forEach>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div id="table2" class="collapse table-responsive">
                        <br>
                        <h5> VISITE SPECIALISTICHE </h5>
                        <br>
                        <table id="specialisticVisitTable" class="table">
                            <caption>List of Specialistic Visit</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Data</th>                        
                                    <th scope="col">Nome</th>
                                    <th scope="col">Esito</th>
                                    <th scope="col">Download</th>
                                    <th scope="col">Pagato</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="specialisticVisit" items="${allSpecialisticVisit}" varStatus="loop">
                                    <tr>
                                        <th scope="row">${loop.count}</th>
                                        <td>${specialisticVisit.dateTime}</td>

                                        <td scope="row">${specialisticVisit.name}</td>
                                        <td scope="row">
                                            <c:if test="${not empty specialisticVisit.anamnesis}">
                                                <button  type="button" class="btn btn-outline-primary" onclick="controllSpecialisticVisit(${specialisticVisit.id})">CONTROLLA</button>
                                            </c:if>
                                            <c:if test="${empty specialisticVisit.anamnesis}">
                                                NON DISPONIBILE
                                            </c:if>
                                        </td>

                                        <td scope="row">    
                                            <c:if test="${not empty specialisticVisit.anamnesis}">
                                                <a target="_blank" href="downloadAnamnesis.html?specialisticVisitName=${specialisticVisit.name}&anamnesis=${specialisticVisit.anamnesis}&visitid=${specialisticVisit.id}&date=${specialisticVisit.dateTime}">
                                                    <img  style="max-height: 30px; max-width: 30px" src="images/icon/pdf.png"></i>
                                                </a>
                                            </c:if>
                                            <c:if test="${empty specialisticVisit.anamnesis}">
                                                NON DISPONIBILE
                                            </c:if>
                                        </td>
                                        <td scope="row">
                                            <c:choose>
                                                <c:when test = "${specialisticVisit.payed.equals(false)}">
                                                    <img  style="max-height: 30px; max-width: 30px" src="images/icon/cross.png">
                                                </c:when>
                                                <c:otherwise>
                                                    <img style="max-height: 30px; max-width: 30px" src="images/icon/accepted.png"></small>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div id="table3" class="collapse table-responsive">
                        <br>
                        <h5> ESAMI </h5>
                        <br>
                        <table id="examTable" class="table">
                            <caption>List of Exam</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">Esame</th>
                                    <th scope="col">Download</th>
                                    <th scope="col">Fatta</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="exam" items="${allExam}" varStatus="loop">
                                    <tr>
                                        <th scope="row">${loop.count}</th>
                                        <td>${exam.examDate}</td>
                                        <td>${exam.name}</td> 
                                        <td scope="row">
                                            <c:choose>
                                                <c:when test = "${exam.made.equals(false)}">
                                                    <p>NON DISPONIBILE</p>
                                                </c:when>
                                                <c:otherwise>
                                                    <a target="_blank" href="downloadEsame.html?nameExam=${exam.name}&dateExam=${exam.examDate}">
                                                        <img  style="max-height: 30px; max-width: 30px" src="images/icon/pdf.png"></i>
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td scope="row">
                                            <c:choose>
                                                <c:when test = "${exam.made.equals(false)}">
                                                    <img  style="max-height: 30px; max-width: 30px" src="images/icon/cross.png">
                                                </c:when>
                                                <c:otherwise>
                                                    <img style="max-height: 30px; max-width: 30px" src="images/icon/accepted.png"></small>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                </div>
                <!--########## VIEW MEDICINE ##########-->
                <div id="containerViewMedicine-hide">
                    <div  class="table-responsive">
                        <br>
                        <h5> MEDICINE </h5>
                        <br>
                        <table id="medicineTable" class="table">
                            <caption>List of Medicine</caption>
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Quantità</th>
                                    <th scope="col">Download ricetta</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="medicine" items="${allMedicine}" varStatus="loop">
                                    <tr>
                                        <th scope="row">${loop.count}</th>
                                        <td>${medicine.name}</td>
                                        <td>${medicine.brand}</td>
                                        <td>${medicine.quantity}</td>
                                        <td scope="row">    
                                            <a target="_blank" href="downloadRicetta.html?prescriptionId=${medicine.id}&medicineName=${medicine.name}&medicineBrand=${medicine.brand}&medicineQuantity=${medicine.quantity}&medicineDate=${medicine.dateTime}">
                                                <img  style="max-height: 30px; max-width: 30px" src="images/icon/pdf.png"></i>
                                            </a>
                                        </td>
                                    </c:forEach>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <script>
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $(document).ready(function () {

            $("#containerChangeMedic-hide").hide();
            $("#containerViewVisitAndExam-hide").hide();
            $("#containerViewMedicine-hide").hide();
            var countHome = 0;
            var countMedicAvailable = 1;
            var countViewVisitAndExam = 1;
            var countViewMedicine = 1;
            $("#cardchose").click(function () {
                $("#containerChangeMedic-hide").hide();
                $("#containerViewVisitAndExam-hide").hide();
                $("#containerViewMedicine-hide").hide();
                countMedicAvailable = 1;
                countViewVisitAndExam = 1;
                countViewMedicine = 1;
                if (countHome % 2 != 0) {
                    $("#containerHome-hide").show();
                    countHome = countHome + 1;
                }
            });
            $("#medicChange").click(function () {
                $("#containerHome-hide").hide();
                $("#containerViewVisitAndExam-hide").hide();
                $("#containerViewMedicine-hide").hide();
                countHome = 1;
                countViewVisitAndExam = 1;
                countViewMedicine = 1;
                if (countMedicAvailable % 2 != 0) {
                    $("#containerChangeMedic-hide").show();
                    countMedicAvailable = countMedicAvailable + 1;
                }
            });
            $("#viewVisitAndExam").click(function () {
                $("#containerHome-hide").hide();
                $("#containerChangeMedic-hide").hide();
                $("#containerViewMedicine-hide").hide();
                countHome = 1;
                countMedicAvailable = 1;
                countViewMedicine = 1;
                if (countViewVisitAndExam % 2 != 0) {
                    $("#containerViewVisitAndExam-hide").show();
                    countViewVisitAndExam = countViewVisitAndExam + 1;
                }
            });
            $("#viewMedicine").click(function () {
                $("#containerHome-hide").hide();
                $("#containerChangeMedic-hide").hide();
                $("#containerViewVisitAndExam-hide").hide();
                countHome = 1;
                countMedicAvailable = 1;
                countViewVisitAndExam = 1;
                if (countViewMedicine % 2 != 0) {
                    $("#containerViewMedicine-hide").show();
                    countViewMedicine = countViewMedicine + 1;
                }
            });


            $('#visitTable').DataTable();
            $('#specialisticVisitTable').DataTable();
            $('#examTable').DataTable();
            $('#medicineTable').DataTable();
        });







        $('#logoutModal').on('shown.bs.modal', function () {
            $('#logoutButton').trigger('focus')
        });

        function controllTheVisit(visitId) {
            jQuery.get("/health_services_system/viewVisitServlet", {"visitId": visitId}, function (data) {
                modifyModal(data);
            });

        }

        function controllSpecialisticVisit(specialisticVisitId) {
            jQuery.get("/health_services_system/viewSpecialisticVisitServlet", {"specialisticVisitId": specialisticVisitId}, function (data) {
                modifyModal2(data);
            });
        }

        function modifyModal(data) {
            var oldCard = "";

            oldCard += '\n\
                   <div id="viewModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> \n\
                    <div class="modal-dialog modal-lg"> \n\
                        <div class="modal-content">\n\
                            <div class="modal-header">\n\
                                <h5 class="modal-title">VISITA</h5>\n\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n\
                                    <span aria-hidden="true">&times;</span>\n\
                                </button>\n\
                            </div>\n\
                            <div class="modal-body">\n\
                                <p>' + data.description + '</p>\n\
                                </div>\n\
                            </div>\n\
                        </div>   \n\
                    </div>'
            $("#viewModal").replaceWith(oldCard);
            $("#viewModal").modal();
        }

        function modifyModal2(data) {
            var oldCard = "";

            oldCard += '\n\
                   <div id="viewModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> \n\
                    <div class="modal-dialog modal-lg"> \n\
                        <div class="modal-content">\n\
                            <div class="modal-header">\n\
                                <h5 class="modal-title">VISITA SPECIALISTICA</h5>\n\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n\
                                    <span aria-hidden="true">&times;</span>\n\
                                </button>\n\
                            </div>\n\
                            <div class="modal-body">\n\
                                <p>' + data.anamnesis + '</p>\n\
                                </div>\n\
                            </div>\n\
                        </div>   \n\
                    </div>'
            $("#viewModal").replaceWith(oldCard);
            $("#viewModal").modal();
        }


        $(".passwordForm[type=password]").keyup(function () {

            if ($("#password1").val() == '' || $("#password2").val() == '') {
                $("#buttonPassword").attr("disabled", true);
                $("#segnalPassword").text("Almeno un campo password vuoto");
            } else if ($("#password1").val() == $("#password2").val()) {
                $("#buttonPassword").attr("disabled", false);
                $("#segnalPassword").text("Ok");
            } else {
                $("#buttonPassword").attr("disabled", true);
                $("#segnalPassword").text("Le password non corrispondono");
            }
        });




    </script>


</body>
</html>
