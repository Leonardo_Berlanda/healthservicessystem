<%-- 
    Document   : Medic
    Created on : 29 ott 2019, 20:33:43
    Author     : aless
--%>

<%@page import="java.util.List"%>
<%@page import="com.mycompany.persistence.entities.Patient"%>
<%@page import="com.mycompany.persistence.entities.Patient"%>
<%@page import="com.mycompany.persistence.entities.Medic"%>
<%@page import="com.mycompany.servlets.LoginServlet"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="com.mycompany.persistence.entities.dao.factory.DAOFactoryException"%>
<%@page import="com.mycompany.persistence.dao.MedicDAO"%>
<%@page import="com.mycompany.persistence.entities.dao.factory.DAOFactory"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!--DATATABLE-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>



        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/MainMedicPage.css">
        <link rel="stylesheet" type="text/css" href="css/menu.css">
        <title>Medico</title>
        <script>var availableTags = []</script>
        <script>var patients = []</script>
        <%

            List<List<String>> patients = (List<List<String>>) request.getAttribute("patientsAndDates");
            for (int i = 0; i < patients.size(); i++) {
        %>
        <script>availableTags.push("<%=patients.get(i).get(1)%>")</script>
        <script>
            patients.push({"Nome": "<%=patients.get(i).get(0)%>",
                "Cognome": "<%=patients.get(i).get(1)%>",
                "CodiceFiscale": "<%=patients.get(i).get(2)%>",
                "E_Mail": "<%=patients.get(i).get(3)%>",
                "UltimaVisita": "<%=patients.get(i).get(4)%>",
                "UltimaPrescrizione": "<%=patients.get(i).get(5)%>",
                "user_idUser": "<%=patients.get(i).get(6)%>"})
        </script>
        <% }%>
        <script>
            $(document).ready(function () {

                function createTable() {
                    var htmlTableString = "<div class='mt-2 mx-2 collapse table-responsive show' id='div_table'>\n\
            <table id='patientsTable' class='table table-bordered table-striped text-center table-hover'>\n\
                        <thead><tr>\n\
                            <th scope='col'>Nome</th>\n\
                            <th scope='col'>Cognome</th>\n\
                            <th scope='col'>Codice fiscale</th>\n\
                            <th scope='col'>E-mail</th>\n\
                            <th scope='col'>Ultima visita</th>\n\
                            <th scope='col'>Ultima prescrizione</th>\n\
                        </tr></thead><tbody>";
                    var counter = 0;

                    availableTags.forEach(availableSurname => {
                        var patientCardUrl = 'MedicServlet/SchedaPaziente?patientId=' + patients[counter].user_idUser;
                        htmlTableString += "\n\
                                        <tr class='clickable-row' data-href=" + patientCardUrl + " style='cursor: pointer;'>\n\
                                            <td>" + patients[counter].Nome + "</td>\n\
                                            <td>" + patients[counter].Cognome + "</td>\n\
                                            <td>" + patients[counter].CodiceFiscale + "</td>\n\
                                            <td>" + patients[counter].E_Mail + "</td>\n\
                                            <td>" + patients[counter].UltimaVisita + "</td>\n\
                                            <td>" + patients[counter].UltimaPrescrizione + "</td>\n\
                                        </tr>"
                        counter++;
                    });
                    htmlTableString += "</tbody></div>"

                    $("#div_table").replaceWith(htmlTableString);
                    $(".clickable-row").click(function () {
                        console.log("CLICKED");
                        window.location.assign($(this).data("href"));
                    });
                }
                createTable();
                var dataSrc = [];
                $('#patientsTable').DataTable({
                    'initComplete': function () {
                        var api = this.api();

                        // Populate a dataset for autocomplete functionality
                        // using data from first, second and third columns
                        api.cells('tr', [0, 1, 2]).every(function () {
                            // Get cell data as plain text
                            var data = $('<div>').html(this.data()).text();
                            if (dataSrc.indexOf(data) === -1) {
                                dataSrc.push(data);
                            }
                        });

                        // Sort dataset alphabetically
                        dataSrc.sort();

                        // Initialize Typeahead plug-in
                        $('.dataTables_filter input[type="search"]', api.table().container())
                                .typeahead({
                                    source: dataSrc,
                                    afterSelect: function (value) {
                                        api.search(value).draw();
                                    }
                                });
                    }
                });
            });
        </script>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <div style="padding-right: 20px">
                <p class="lead">${medic.getName()} ${medic.getSurname()}</p>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/health_services_system/medic">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li>
                        <button class="mx-2 btn btn-primary" data-toggle="modal" data-target="#changePasswordModal" >Cambia Password</button>
                    </li>
                    <a id="logouttt" class="btn btn-danger" role="button" href="/health_services_system/logout.handler">LogOut</a>
                </ul>
            </div>
        </nav>
        <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">CAMBIA PASSWORD</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-8">

                                <form style="min-width: 100%; background-color: white; " type="submit" action="changePassword" method="post">
                                    <input  name="username" class="invisible" value="${medic.getUserName()}">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password Corrente</label>
                                        <input name="oldPassword" type="password" class="form-control" id="exampleInputPassword1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Nuova Password</label>
                                        <input  name="newPassword" id="password1" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Ripeti la Password</label>
                                        <input  id="password2" type="password" class="passwordForm form-control" id="exampleInputPassword1">
                                    </div>
                                    <p id="segnalPassword">Password vuota</p>
                                    <button id="buttonPassword"  disabled="true"  type="submit" class="btn btn-primary">Submit</button>

                                </form>

                            </div>  
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="raw mt-5" id="div_table">

        </div>
        <script>$(".passwordForm[type=password]").keyup(function () {

                if ($("#password1").val() == '' || $("#password2").val() == '') {
                    $("#buttonPassword").attr("disabled", true);
                    $("#segnalPassword").text("Almeno un campo password vuoto");
                } else if ($("#password1").val() == $("#password2").val()) {
                    $("#buttonPassword").attr("disabled", false);
                    $("#segnalPassword").text("Ok");
                } else {
                    $("#buttonPassword").attr("disabled", true);
                    $("#segnalPassword").text("Le password non corrispondono");
                }
            });</script>
    </body>
</html>
