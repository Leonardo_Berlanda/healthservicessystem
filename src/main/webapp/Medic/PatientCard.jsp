<%-- 
    Document   : PatientCard
    Created on : 26 dic 2019, 22:51:04
    Author     : aless
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!--DATATABLE-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>



        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/PatientCard.css">

        <script type="text/javascript" src="../js/PatientCard.js"></script>
        <title>Scheda paziente</title>
    </head>
    <body>
        <script>
            var visitsJson = ${visitsJson};
            var idPatient = "${param.patientId}";
            $(document).ready(function () {
                setJsonVisits(visitsJson);
                createExamsCards(idPatient, 1);
            });
        </script>
        <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light border-bottom">
            <div style="padding-right: 20px">
                <p class="lead">${medic.getName()} ${medic.getSurname()}</p>
            </div>

            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Visite
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" onclick="createTable(visitsJson, idPatient)" href="#">Visite</a>
                    <a class="dropdown-item" onclick="createExamsCards(idPatient, 1)" href="#">Esami</a>
                </div>
            </div>

            <div>
                <a class="btn btn-primary" style="margin-left: 40px" href="CreaVisita?patientId=${param.patientId}" role="button">Nuova visita</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/health_services_system/medic">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <a id="logouttt" class="btn btn-danger" role="button" href="/health_services_system/logout.handler">LogOut</a>
                </ul>
            </div>
        </nav>


        <div class="container-fluid">
            <div class="row">
                <div id="mySidenav" class="sidenav col-lg-3">
                    <div class="row">
                        <p class="col-lg-12 col-12 lead">PAZIENTE</p>
                        <object data="../images/profile/profile_picture_${patient.getUserName()}.jpg"  class="col-lg-12 col-3 img-fluid border border-secondary img-thumbnail" alt="Responsive image">
                            <img src="../images/unknownperson.png" alt="Stack Overflow logo and icons and such"  class="col-lg-12 col-3 img-fluid border border-secondary img-thumbnail" alt="Responsive image">
                        </object>
                        <div class="row col-lg-12 col-9">
                            <p class="col-lg-12 col-6 lead text-truncate">${patient.getName()} ${patient.getSurname()}</p>
                            <p class="col-lg-12 col-6 lead text-truncate">${patient.getFiscalCode()}</p>
                            <p class="col-lg-12 col-6 lead text-truncate">${patient.getSex()}</p>
                            <p class="col-lg-12 col-6 lead text-truncate">${patient.getEmail()}</p>
                        </div>
                    </div>
                </div>

                <div id="content" class="col-lg-9"></div>
            </div>
        </div>
    </body>
</html>
