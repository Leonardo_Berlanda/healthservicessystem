<%-- 
    Document   : CreateVisit
    Created on : 30 dic 2019, 19:30:34
    Author     : aless
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="now" class="java.util.Date"/> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="../css/wickedpicker.css">
        <script type="text/javascript" src="../js/wickedpicker.js"></script>

        <script src="../js/bootstrap-input-spinner.js"></script>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!--DATATABLE-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>


        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/SidePatientInformations.css">
        <link rel="stylesheet" href="../css/CreateVisit.css">

        <script type="text/javascript" src="../js/CreateVisit.js"></script>
        <title>Crea visita</title>
    </head>
    <body>
        <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light border-bottom">
            <div style="padding-right: 20px">
                <p class="lead">${medic.getName()} ${medic.getSurname()}</p>
            </div>

            <div>
                <a class="btn btn-primary text-white" id="saveVisitButton" style="margin-left: 40px" onclick='saveVisit("${param.patientId}")' role="button">Salva</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/health_services_system/medic">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/health_services_system/MedicServlet/SchedaPaziente?patientId=${patient.getUserName()}">Scheda paziente <span class="sr-only">(current)</span></a>
                    </li>
                    <a id="logouttt" class="btn btn-danger" role="button" href="/health_services_system/logout.handler">LogOut</a>
                </ul>
            </div>
        </nav>


        <div class="container-fluid">
            <div class="row">
                <div id="mySidenav" class="sidenav col-lg-3">
                    <div class="row">
                        <p class="col-lg-12 col-12 lead">PAZIENTE</p>
                        <object data="../images/profile/profile_picture_${patient.getUserName()}.jpg"  class="col-lg-12 col-3 img-fluid border border-secondary img-thumbnail" alt="Responsive image">
                            <img src="../images/unknownperson.png" alt="Stack Overflow logo and icons and such"  class="col-lg-12 col-3 img-fluid border border-secondary img-thumbnail" alt="Responsive image">
                        </object>
                        <div class="row col-lg-12 col-9">
                            <p class="col-lg-12 lead col-6">${patient.getName()} ${patient.getSurname()}</p>
                            <p class="col-lg-12 lead col-6">${patient.getFiscalCode()}</p>
                            <p class="col-lg-12 lead col-6">${patient.getSex()}</p>
                            <p class="col-lg-12 lead col-6">${patient.getEmail()}</p>
                        </div>
                    </div>
                </div>

                <div id="content" class="col-lg-9">

                    <div id='cards'>
                        <div id="visitCard" class='mx-4'>
                            <div id='cardVisitContainer' class='row'>
                                <div class='col-sm-12 mt-4'>
                                    <div class='card'>
                                        <div class='card-body'>
                                            <h5 class='card-title' id="dateVisit"><fmt:formatDate value="${now}" pattern="dd-MM-yyyy" /></h5>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Anamnesi visita:</label>
                                                <textarea class="form-control" id="anamnesysTextArea" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="cardContent" class='mx-4'>
                            <div id='cardSpecialVisitContainer' class='row'>
                                <div class='col-lg-4 my-4'>
                                    <div class='card' id="cardButtons">
                                        <div class="row">
                                            <h5 class='card-title m-1 col-sm-12'>Aggiungi</h5>
                                        </div>
                                        <div class='card-body' id="addCard">
                                            <div class="form-group row">
                                                <button type="button" onclick="addSpecialisticVisit()" class="btn btn-outline-secondary my-2 col-sm-12 text-truncate">Visita specialistica
                                                </button>
                                                <button type="button" onclick="addMedicinePrescription()" class="btn btn-outline-secondary my-2 col-sm-12 text-truncate">Prescrizione farmaco 
                                                </button>
                                                <button type='button' onclick='addExamSSP()' class='btn btn-outline-secondary my-2 col-sm-12 text-truncate'>Esame SSP
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </body>
</html>